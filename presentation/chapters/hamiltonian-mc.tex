% =============================================================================
% Chapter covers the general definition of the Hamiltonian Monte Carlo as 
% abstract steps pointing towards choices of optimization as different 
% integrators, kinetic energies etc.
% =============================================================================

\begin{frame}[fragile]
    \frametitle{Hamiltonian Monte Carlo Sampling Step}

    \begin{algorithm}[H]
        \SetAlgoLined
        \algsetup{linenosize=\tiny}
        \scriptsize
        \begin{algorithmic}[1]

            % init the position and momentum variable 
            \STATE $q_t \gets q_{t-1}$ \\
            \STATE $p_t \sim \mathcal{N}(0,M),\ p_{t'} \gets p_t$  \\
            \STATE

            \visible<2->{
                % integrate over phase space 
                \FOR{$1$ to $L$}
    
                    % updates according to Leapfrog 
                    \STATE $p_{t + \epsilon / 2}                                            \gets p_t + \frac{\epsilon}{2} \frac{dp_t}{dt} = p_t - \epsilon \frac{\partial U}{\partial q_t}(q_t)$  \\
                    \STATE $\hphantom{p_{t + \epsilon / 2}} \mathllap{q_{t + \epsilon}}     \gets q_t + \epsilon \frac{dq_t}{dt} = q_t + \epsilon \frac{\partial K}{\partial p}$  \hfill \COMMENT{$K(p) = p^T M^{-1} p / 2 \to p^T M^{-1}$}  \\
                    \STATE $\hphantom{p_{t + \epsilon / 2}} \mathllap{p_{t + \epsilon}}     \gets p_{t + \epsilon / 2} + \frac{\epsilon}{2} \frac{dp_t}{dt} = p_t - \epsilon \frac{\partial U}{\partial q_t}(q_{t + \epsilon})$  \\
    
                \ENDFOR
                \STATE
            }

            \visible<3->{
                % negate momentum for symmetry 
                \STATE $p_{L * \epsilon} \gets -p_{L * \epsilon}$  \\ 
    
                % compute metropolis update 
                \STATE $r \gets \min\lbrace{1, \ \exp{ \lbrace H(q_{t-1}, p_{t'}) - H(q_{L * \epsilon}, p_{L * \epsilon})} \rbrace } \rbrace$  \\
                \STATE
    
                \IF {$\mathcal{U}(0,1) \leq r$}
                    \RETURN $q_{L * \epsilon}$  \\
                \ELSE
                    \RETURN $q_{t-1}$  \\
                \ENDIF 
            }

        \end{algorithmic}
        \caption{ One step of HMC traversing the phase-space, using Leapfrog integration and a $\pi(p \vert q) \sim \mathcal{N}(0,M)$\cite{mcmcHandbook}. }
        \label{alg:seq}
    \end{algorithm}

\end{frame}

\begin{frame}
    \frametitle{Properties of the HMC}

    \begin{itemize}
        \item Conservation of Hamiltonian during exploration
        \item Volume preservation in phase-space 
        \item Invariance
        \item Ergodicity
            
    \end{itemize}

\end{frame}

\begin{frame}
    \frametitle{Properties of the HMC}

    \begin{itemize}
        \item Conservation of Hamiltonian during exploration
            \begin{itemize}
                \item by the total derivative w.r.t. $t$ we get
                    \begin{align}
                        \frac{dH}{dt} &= \sum_i \left[ \frac{\partial H}{\partial q_i}\frac{dq_i}{dt} 
                                + \frac{\partial H}{\partial p_i}\frac{dp_i}{dt} \right]                    \nonumber \\
                        &= \sum_i \left[ \frac{\partial H}{\partial q_i}\frac{\partial H}{\partial p_i} 
                                - \frac{\partial H}{\partial p_i}\frac{\partial H}{\partial q_i} \right]    \nonumber \\
                        &= 0  \label{eq:conservation-volume}
                    \end{align}
                \item errors are introduced due integration
            \end{itemize}
        \item Volume preservation in phase-space 
        \item Invariance
        \item Ergodicity
            
    \end{itemize}

\end{frame}

\begin{frame}
    \frametitle{Properties of the HMC}

    \begin{itemize}
        \item Conservation of Hamiltonian during exploration
        \item Volume preservation in phase-space 
            % NOTE: think about adding symplecticness 
            % during exploration the variables can change increasing or decreasing the region
            % in one direction; but the Hamiltonian equations take care to squash/stretch the 
            % region in the other direction to account for such changes
            % if we would NOT use the Hamiltonian, we'd have to compute the determinant of the 
            % Jacobian for mapping the dynamics define -> probably impossible 
            \begin{itemize}
                \item using the divergence of the vector field $\mathbf{V} = \left( \frac{dq}{dt}, \frac{dp}{dt} \right)$
                \begin{align}
                    \operatorname{div} \mathbf{V} &= \sum_i \left[ \frac{\partial}{\partial q_i} \frac{dq_i}{dt} 
                            + \frac{\partial}{\partial p_i} \frac{dp_i}{dt} \right]                 \nonumber \\
                    &= \sum_i \left[ \frac{\partial}{\partial q_i} \frac{\partial H}{\partial p_i} 
                            - \frac{\partial}{\partial p_i} \frac{\partial H}{\partial q_i} \right]  \nonumber \\
                    &= 0 \label{eq:div-vector-field}
                \end{align}
                \item $\to$ we don't have to account for changing vol. in acceptance step
            \end{itemize}
        \item Invariance
        \item Ergodicity
            
    \end{itemize}

\end{frame}

\begin{frame}
    \frametitle{Properties of the HMC}

    \begin{itemize}
        \item Conservation of Hamiltonian during exploration
        \item Volume preservation in phase-space 
        \item Invariance
            % Reversibility/detailed balance is a sufficient condition 
            % for invariance; there are two things to it, 
            % the Hamiltonian and the integration technique, 
            % i.e. Leapfrogging as the Ham. stays invariant, the 
            % integration technique can introduce errors yielding unequal 
            % Hamiltonians as otherwise. the proposed step would always be accepted
            % the errors are introduced by the discretization to some small eps
            % if eps -> 0 then also err -> 0 as bound
            \begin{itemize}
                \item follows from (\ref{eq:conservation-volume}) and (\ref{eq:div-vector-field})
                \item recall, joint distribution (\ref{eq:canonical-distribution}) only depends on $H(q,p)$
                \item open: integrator needs to conserve detailed balance
            \end{itemize}
        \item Ergodicity
            
    \end{itemize}

\end{frame}

\begin{frame}
    \frametitle{Properties of the HMC}

    \begin{itemize}
        \item Conservation of Hamiltonian during exploration
        \item Volume preservation in phase-space 
        \item Invariance
        \item Ergodicity
            \begin{itemize}
                \item due to sampling momentum $p$, affecting $q$ in arb. ways
                \item can fail, if integration steps yield exact periodicity, e.g. $L\epsilon = 2\pi$ for a circle
                \item solution: vary $\epsilon$ or $L$ by small amounts \cite{Mackenzie:1989us}
            \end{itemize}
            
    \end{itemize}

\end{frame}

\begin{frame}
    \frametitle{Benifit of Avoiding Local Random Walks}

    % model with high correlation in the position variables and a std. Gaussian
    % kinetic energy.
    % for the Random Walk Metropolis, the changes need to be ~ to the std. deviation
    % of the more constraint dimension, i.e ~0.14 as 0.14 the sqrt of the 2nd eigenvalue
    % Changes produces by Gibbs sampling are in the same order 
    % iterations needed to reach a indep. state is determined by how long it takes to
    % explore the less constrained direction -> having std. dev. 1.41, i.e. 10x
    % the random walk metropolis will not need just 10 steps but more likely 100 due to
    % its distance traveled prop. to sqrt(n)
    % for the HMC to keep a high acceptance rate, the step size should be ~ .14 as it
    % is the std. of the more constrained direction; as HMC moves straight, it needs
    % roughly 10 steps, i.e. the adv. is 10x better 

    given the model:

    \[
        H(q,p) = \frac{q^T \Sigma^{-1} q}{2} + \frac{p^Tp}{2}, \quad \Sigma = \begin{bmatrix}
            1 & .98  \\
            .98 & 1
        \end{bmatrix}
    \]

    \begin{itemize}
        \item high acceptance prob. for RW-Metropolis and HMC $\to$ $\vert \epsilon \vert \sim 0.14$
        \item less constrained dimension has $\sigma = 1.41 \to 10\times$ 
        \item $n_{RWM} = 100$ iterations to reach almost indep. state
        \item advantage of HMC in the order of $10x$
    \end{itemize}

\end{frame}

\begin{frame}
    \frametitle{Visualization of HMC Sampling}

    % emphasize the large distances HMC can travel while still producing 
    % an accepted proposal. 
    % show the Metropolis alg. in comparison and how "slowly" it traverses 
    % the space.

    \begin{figure}
        \includegraphics[width=1.\linewidth]{hmc_visualization.jpg}
        \caption{Visualization of several sampling algorithms in \textit{"The Markov-chain Monte Carlo Interactive Gallery"}\cite{gitHubSimulation}.}
    \end{figure}

\end{frame}