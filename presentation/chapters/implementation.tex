% =============================================================================
% Covers the the algorithm in practice such as different choices of kinetic 
% energies, integrators and such. Also, the short comings and possible pitfalls 
% are covered.
% =============================================================================

\begin{frame}
    \frametitle{Integration methods}

    % HMC builds heavily on the construction of numerically stable integrators
    % as Hamilton's equations cannot be solved exactly and need to be numerically
    % approximated. inaccuracies in the integration compromise the utility of 
    % the HMC as a whole. 
    % Solving the Hamiltonian equations on the vector field is equivalent to solving 
    % a system of ordinary differential equations on phase space -> the more 
    % accurate the solution, the more effective the the implementation
    % generic numerical integrators often suffer fom the drift phenomenon and 
    % thus being limited to approximating only short trajectories.

    \begin{figure}
        \includegraphics[width=1.\linewidth]{integration_methods.png}
        \caption{Comparison of common integration techniques on a simple circle - 
            $H(q,p) = \frac{q^2}{2} + \frac{p^2}{2}$. Using $\epsilon = .3$ for each. 
            Euler's method is showing a drift - common phenomenon of generic integrators.}
        \label{fig:integrator-comparisons}
    \end{figure}

\end{frame}

\begin{frame}
    \frametitle{Symplectic Integrators $\to$ Leapfrog Integration}

    % symplectic integrators preserve the volume of phase space, just as the 
    % Hamiltonian trajectories, which they are approximating. Therefore, they
    % cannot deviate from the energy of the original trajectory too far.
    % They will show a oscillation around the trajectory - even for ling times.
    % For the leapfrog integrators, only shearing transformations are applied
    % which are volume preserving, als transformation only one variable w.r.t.
    % other.
    % If the momentum distribution is indep. of the position the Leapfrog 
    % integrator can be used, i.e. the case for std. Gaussian Energy

    If $p \perp q$ the Leapfrog integrator can be used.

    \begin{figure}
        \includegraphics[width=\linewidth]{leapfrog_variations.png}
        \caption{The quality of Leapfrog integration depends on the step size 
            $\epsilon$. The trajectory remains stable while $\epsilon$ is increased. 
            But as step sizes increase the trajectories become unstable. \cite{mcmcHandbook}
            Using $\epsilon \in \{.1, 1.2, 1.8\}$}
    \end{figure}

\end{frame}

\begin{frame}
    \frametitle{Divirgence of Symplectic Integrators}

    % in areas of high curvature even symplectic integrators diverge towards the 
    % boundaries of phase space. 
    % this behavior is easily spotted compared to the slow drift of generic integrators.

    \begin{figure}
        \includegraphics[width=.7\linewidth]{diverging_symplectic_integrator.png}
        \caption{In areas of high curvature, symplectic integrators diverge towards the 
            boundaries of phase space. Unlike the slow drift of generic integrators are 
            easily spotted in practice. \cite{betancourtConceptional}}
        \label{fig:diverging-symplectic-int}
    \end{figure}

\end{frame}

\begin{frame}
    \frametitle{Accounting for Integration Error}

    % --> the reason for the Metropolis step
    % Even symplectic integrators introduce small errors due to numerical integration 
    % which bias the resulting Hamiltonian transitions.
    % If there was no error, the acceptance of a proposal would be 1, always.
    % Correction by treating the Hamiltonian transition as a proposal for Metropolis 
    % Hastings on phase space. 
    % To ensure that the Metropolis Hastings acceptance probability does not vanish, 
    % the momentum is inverted after the HMC step yielding reversible transitions.
    % Adding the negation does not change the Hamiltonian as the trajectory can be 
    % explored in both directions.

    Symplectic integrators still introduce small errors, biasing resulting Hamiltonian 
        transitions \footnotemark
    % \linebreak

    Making integration \textit{reversible} by adding negation step
    \[
        (q,p) \to (q, -p)  
    \]

    Yielding a reversible proposal
    \begin{align*}
        p(q_L,p_L \vert q_0, p_0) &= \min \left( 1, \frac{ \mathbb{Q}(q_0, p_0 \vert q_L, -p_L) \pi(q_L, -p_L) }
            { \mathbb{Q}(q_L, -p_L \vert q_0, p_0)\pi(q_0, p_0)} \right)           \\
        &= \min \left( 1, \frac{\pi(q_L, -p_L)}{\pi(q_0, p_0)} \right)              \\
        &= \min \left( 1, \frac{\exp -H(q_L, -p_L)}{\exp -H(q_0, p_0)} \right)      \\
        &= \min \left( 1, \exp \lbrace -H(q_L, -p_L) + H(q_0, p_0) \rbrace \right)
    \end{align*}

    \footnotetext[1]{While only covering first-order Leapfrog, active research suggest using 
        higher-order integrators, esp. on higher dimensional problems, \cite{numericalIntegratorsHMC,
        numericalIntegratorsFernandez}}

\end{frame}

\begin{frame}

    % for the following slides, note that there a basically three spots for tuning the 
    % the algorithm besides the integrator
    %   1) distribution of the kinetic energy
    %   2) Step sizes 
    %   3) Trajectory length
    % 

    \frametitle{Choice of Kinetic Energy}
    
    % reason non-Gaussians perform poorly:
    %   w/o further assumption the marginal energy distribution
    %   will follow a Gaussian in high-dim spaces due to central
    %   limit theorem.

    \begin{itemize}
        \item Euclidean-Gaussian Kinetic Energy
            \[
                \pi(p \vert q) = \mathcal{N}(p \vert 0, M)  
            \]
        \item Riemannian-Gaussian Kinetic Energy
            \[
                \pi(p \vert q) = \mathcal{N}(p \vert 0, \Sigma(q))  
            \]
        \item Non-Gaussian Kinetic Energy; empirically perform poorly
    \end{itemize}

\end{frame}

% \begin{frame}
%     \frametitle{Choosing a Stepsize $\epsilon$}

%     \textcolor{red}{TODO} \dots

% \end{frame}

\begin{frame}
    \frametitle{Choosing a Trajectoty Length $L$ and Steps Size $\epsilon$}

    depends on problem $\to$ trial and error
    
    recommendations: $L = 100$, increase if run is autocorrelated, \cite{mcmcHandbook}; or let $\epsilon L = 1$, \cite{GelmanBayesianDA}

    \begin{figure}
        \includegraphics[width=.35\linewidth]{returning_trajectory.jpg}
        \caption{Trajectory on $\pm 1\sigma$ of a 2D Gaussian with $L=25$, 
            $\epsilon=.25$. \cite{mcmcHandbook}}
        \label{fig:sample-trajectory}
    \end{figure}

\end{frame}

\begin{frame}
    \frametitle{Using a Warm-up Phase for Parameter Selection}

    % only alter the parameters "greatly" during the warm-up phase as they might
    % break properties of the algorithm as detailed balance 
    %  steps can be repeated several times until good parameters are found; but 
    % it's important to drop all obtained samples during the altering phase 
    \begin{itemize}
        \item drop samples outside of typical set - \textit{burnin-phase}
        \item altering hyperparameters breaks invariance $\to$ only during warm-up phase, \cite{mcmcHandbook}
        \item acceptance ratio should be $\sim 65\%$, \cite{GelmanBayesianDA}
    \end{itemize}    

\end{frame}

\begin{frame}
    \frametitle{Illustration of burnin-phase}

    \begin{figure}
        \includegraphics[width=.8\linewidth]{burning_illustration.png}
        \caption{Burnin-phase is set to the first $100$ samples which are dropped to not 
            bias inference.}
        \label{fig:burnin-phase}
    \end{figure}

\end{frame}
