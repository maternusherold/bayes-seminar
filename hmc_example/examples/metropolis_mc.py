import numpy as np 
from tqdm import tqdm

from scipy.stats import multivariate_normal as mv

# global param 
dim_ = 3


def fprob(x, mu=np.zeros(dim_), std=np.eye(dim_)):
    """Simple log. prob. evaluation. """
    return mv.logpdf(x, mu, std)


def run_metropolis(evaluateLogLikelihood=fprob, steps=1000, step_size=.01, dim_=dim_):
    """Metropolis Hastings sampler. """
    # initial guess for alpha as array.
    guess = np.random.randn(dim_)
    # Prepare storing MCMC chain as array of arrays.
    A = np.zeros((steps, dim_))
    A[0] = np.array([guess])

    # define stepsize of MCMC.
    stepsizes = np.eye(dim_) * step_size  # array of stepsizes
    accepted  = 0.0

    # Metropolis-Hastings with 10,000 iterations.
    for n in tqdm(range(1, steps)):
        old_alpha  = A[n,:]  # old parameter value as array
        old_loglik = evaluateLogLikelihood(old_alpha)

        # Suggest new candidate from Gaussian proposal distribution.
        new_alpha = mv(old_alpha, stepsizes).rvs()
        assert new_alpha.shape == guess.shape, f"shape missmatch! got \   {new_alpha.shape} vs {guess.shape}"

        new_loglik = evaluateLogLikelihood(new_alpha)
        # Accept new candidate in Monte-Carlo fashing.
        if (new_loglik > old_loglik):
            A[n] = new_alpha
            accepted = accepted + 1.0  # monitor acceptance
        else:
            u = np.random.rand(dim_)
            if (u < np.exp(new_loglik - old_loglik)).all():
                A[n] = new_alpha
                accepted = accepted + 1.0  # monitor acceptance
            else:
                A[n] = old_alpha

    print(f"Acceptance rate = {accepted / steps:.2f}")
    return A


if __name__=="__main__":
    samples = run_metropolis()
    print(f"sample means: {samples.mean(axis=0)}")
