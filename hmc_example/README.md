# Hamiltonian Monte Carlo - implementation

_Implementation of the HMC algorithm for a dedicated example_

each examples is - except for general functions from `utils_sampling.py` - a standalone program per directory. 

```
.
├── data
├── hmc_boston
├── hmc_endometrial
├── hmc_students_alcohol
├── __pycache__
├── README.md
├── __init__.py
├── integration_methods.py
├── integration_techniques
├── hamiltonian_monte_carlo.py
└── utils_sampling.py

7 directories, 6 files
```

