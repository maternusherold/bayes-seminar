% =============================================================================
% Chapter describes the basics for the algorithm, i.e. Hamiltonian dynamics, 
% the idea of level sets and the traversal of the different energy levels as 
% well as between such. Finally it presents the algorithm in detail.
% =============================================================================

The method itself has its origins in physics and defining it will use several physics terms. For the most of them, however, there are well defined statistical terms as well, e.g. the covariance matrix is called mass matrix. \linebreak

Aiming to incorporate information of where to move next, the Hamiltonian Monte Carlo method adds another variable to the distribution $q$, \textit{pushing} the system into the desired direction - momentum $p$. 

\begin{align}
    q &\to (q, p)                       \label{eq:momentum} \\
    \pi(q,p) &= \pi(q)\pi(p \vert q)    \label{eq:joint-dist}
\end{align}

By adding momentum, a system of energy $E(p,q)$ is defined. The different configurations of $q,p$ yielding the same energy $E$ are defined in the so called \textit{phase space}. While the idea of momentum is to add an extra energy term to help exploring the target space, the amount to add is computed by simulating a system of \textit{conservative dynamics}. This is a system adjusting its parameters to compensate for the change in another variable to maintain the energy level it started with.  

The idea HMC is pursuing with the added momentum is to explore the phase space first by ways defined in physics and deducing information about the target distribution from it. This is possible by how the joint distribution of momentum and distribution is chosen, see \cref{eq:joint-dist}. By marginalizing the momentum and using that $\pi(p \vert q)$ is a valid probability measure, the target distribution is obtained as seen in \cref{eq:marginalization}.

\begin{align}
    \label{eq:marginalization}
    \int \pi(q,p) dp &= \frac{1}{Z} \pi(q) \int \pi(q \vert p) dp  \\
    &= \frac{1}{Z} \pi(q)  \\
    &\propto \pi(q)
\end{align}

The amount of how the variables have to change is defined by the Hamiltonian equations \cref{eq:ham-dynamics1,eq:ham-dynamics2} describing the Hamiltonian dynamics - the origin of the method's name. When changing the variables according to the system, the total energy is kept constant throughout the changes. The total energy is often referred to as the \textit{Hamiltonian energy} $H(q,p)$ instead of the general energy $E(q,p)$. 

\begin{align}
    \frac{dq_i}{dt} &= \frac{\partial H}{\partial p_i} \label{eq:ham-dynamics1} \\
    \frac{dp_i}{dt} &= - \frac{\partial H}{\partial q_i} \label{eq:ham-dynamics2}
\end{align}

The two variables of position and momentum define a joint distribution over the phase space as in \cref{eq:joint-dist} with the total energy $H$. In physics such joint distributions over the parameter space are defined by canonical distributions \cref{eq:canonical-distribution}. By solving the canonical distribution for the total energy the measure is separated into two parts, the potential energy $U(q) = -\pi(q)$ and the kinetic energy $K(q,p) = -\pi(p \vert q)$. As $q$ describes the distribution's parameters, $U$ is also called the negative log posterior in Bayesian settings. 

\begin{align}
    \pi(q,p) &= \frac{1}{Z} \exp{-E}    \label{eq:canonical-distribution} \\
    &\propto \exp{E}            \\
    &= \exp{-H(q,p)}            \\
    H(q,p) &= - \log(q,p)       \label{eq:neg-log-hamiltonian}  \\
    &= U(q) + K(q,p)            \label{eq:hamiltonian}
\end{align}

\subsection{HMC sampler}
\label{subsec:hmc-sampler}

The before mentioned efficiency of the HMC method results from the way in which HMC traverses the phase space. As the Hamiltonian equations from \cref{eq:ham-dynamics1,eq:ham-dynamics2} define detailed steps to take for retaining the current energy level, the sampler moves according to such. Therefore, the Hamiltonian Monte Carlo method divides the phase space into different energy levels, i.e. level sets according to the energy:

\begin{equation}
    H^{-1}(E) = \{q,p \ \vert \ H(q,p) = E\}
\end{equation}. 

On such levels, the sampler moves w.r.t. the Hamiltonian equations and can thus travels far distances without leaving the typical set as introduced in \cref{sec:INTRO}. By movement the integration over the trajectory is meant. The theory on why it could travel forever is discussed in \cref{subsec:hmc-properties}. Each integrated path is one step of the sampler. Using conservative dynamics for the movement eliminates the local random walk. Note, while it's said the sampler has made just one step, as it just returned the next position, it actually has made several, often hundred, steps during integration due to numerical approximation of such. 

Those levels are explored efficiently, utilizing the Hamiltonian equations to glide through the space. To explore between these sets, the HMC sampler uses the position\footnote{Position refers to the distribution's parameters but is more convenient to use as it is more generic and conforms with the notion of a space.} obtained in the previous iteration and computes a new energy level by sampling a momentum randomly from the kinetic energy, a distribution itself. \cref{fig:conotur-level-sets} illustrates the phase space and the levels sets defined by combinations of momentum and position.

Sampling the momenta to jump between the energy levels introduces a random walk behavior between the level sets. Thus, while HMC gets rid of the local random walk, it still incorporates one. 

Therefore, the efficiency in exploring the phase space is determined by the shape of it to which the choice of kinetic energy and settings in the integrator must match. The exploration speed between the energy levels depends on the similarity between the true energy distribution $\pi(E)$ and the conditional energy distribution $\pi(E \vert q)$ obtained by sampling from the kinetic energy.

\begin{equation}
    \label{eq:energy-similarity}
    \pi(E \vert q) \sim \pi(E)
\end{equation}

Having similar distributions in \cref{eq:energy-similarity}, the samples, i.e. the momenta, yield meaningful values for the overall phase space. In the case of a mismatch, many sampled momenta will lead to energy levels being connected to the tails of the target distribution $q$ or being off the typical set completely, i.e. slowing down the exploration. 

\begin{figure}
    \centering
    \includegraphics[width=1.\linewidth]{contourplot_energy_levels.png}
    \caption{The Hamiltonian defines level sets on the phase space which are explored by altering the variables according to the Hamiltonian equations,preserving constant energy during exploration.}
    \label{fig:conotur-level-sets}
\end{figure}

The overall steps for one iteration of Hamiltonian Monte Carlo sampling are listed in \cref{alg:hmc}. For a better understanding, the kinetic energy was chosen to be a standard Gaussian, independent of the position. In general, consequences of different kinetic energies are discussed in \cref{subsec:hyper-kinetic}, in this case it's for understandability. Also, the integration method was chosen to be the leapfrog algorithm as it's the default one in HMC. Integration methods are discussed in \cref{subsec:hmc-integration}. 

\begin{algorithm}
    \SetAlgoLined
    \algsetup{linenosize=\tiny}
    \scriptsize
    \begin{algorithmic}[1]

        % init the position and momentum variable 
        \STATE $q_t \gets q_{t-1}$ \\
        \STATE $p_t \sim \mathcal{N}(0,M),\ p_{t'} \gets p_t$  \\
        \STATE

        % integrate over phase space 
        \FOR{$1$ to $L$}

            % updates according to Leapfrog 
            \STATE $p_{t + \epsilon / 2}                                    \gets p_t + \frac{\epsilon}{2} \frac{dp_t}{dt} = p_t - \epsilon \frac{\partial U}{\partial q_t}(q_t)$  \\

            \STATE $\hphantom{p_{t + \epsilon / 2}} \mathllap{q_{t + \epsilon}} \gets q_t + \epsilon \frac{dq_t}{dt} = q_t + \epsilon \frac{\partial K}{\partial p}$  \hfill \COMMENT{see \cref{eq:algorithm-kinetic}}  \\
            
            \STATE $\hphantom{p_{t + \epsilon / 2}} \mathllap{p_{t + \epsilon}} \gets p_{t + \epsilon / 2} + \frac{\epsilon}{2} \frac{dp_t}{dt} = p_t - \epsilon \frac{\partial U}{\partial q_t}(q_{t + \epsilon})$  \\

        \ENDFOR
        \STATE
        
        % negate momentum for symmetry 
        \STATE $p_{L * \epsilon} \gets -p_{L * \epsilon}$  \\ 

        % compute metropolis update 
        \STATE $r \gets \min\lbrace{1, \ \exp{ \lbrace H(q_{t-1}, p_{t'}) - H(q_{L * \epsilon}, p_{L * \epsilon})} \rbrace } \rbrace$  \\
        \STATE

        \IF {$\mathcal{U}(0,1) \leq r$}
            \RETURN $q_{L * \epsilon}$  \\
        \ELSE
            \RETURN $q_{t-1}$  \\
        \ENDIF 

    \end{algorithmic}
    \caption{ One step of HMC traversing the phase space, using Leapfrog integration and a $\pi(p \vert q) \sim \mathcal{N}(0,M)$\cite{mcmcHandbook}.}
    \label{alg:hmc}
\end{algorithm}

\begin{figure}
    \[ 
        K(p) = p^T M^{-1} p / 2 \to p^T M^{-1}    
    \]
    
    \caption{The update for the momentum in \cref{alg:hmc} results from deriving the Gaussian kinetic energy after the momentum $p$.}
    \label{eq:algorithm-kinetic}
\end{figure}


\subsection{Properties of the HMC}
\label{subsec:hmc-properties}

Key properties of Monte Carlo sampling methods are invariance of the distribution and ergodicity, both described in \cref{sec:INTRO}. Those two properties lead to the desired asymptotic behavior in \cref{eq:mcmc-asymptotic}. To show these the conservation of the Hamiltonian during the exploration phase and volume preservation over the phase space will be used. The first of such is also the theoretic reasoning why the integration could run forever and still yield valid samples from the target distribution $q$.

To ease the proofs one should first imagen the phase space to be covered by a vector field. This is valid as the Hamiltonians add a gradient description to each combination of position and momentum in phase space, yielding the desired vector field. 

Further, the flow of a vector field is calculated as the divergence. Given $f: G \to \mathcal{K}^n$ the divergence of $f$ is defined as 

\begin{equation}
    \label{eq:divergence}
    \operatorname{div} f(\phi) = \sum^n_{i=0} \partial_i f_i(\phi)
\end{equation}, assuming the partial derivatives exist. 

To show \textit{conservation of Hamiltonian} during integration the total derivative of the energy is computed while substituting the definitions of the Hamiltonian equations: 

\begin{align*}
    \frac{dH}{dt} &= \sum_i \left[ \frac{\partial H}{\partial q_i}\frac{dq_i}{dt} + \frac{\partial H}{\partial p_i}\frac{dp_i}{dt} \right] \\
    &= \sum_i \left[ \frac{\partial H}{\partial q_i}\frac{\partial H}{\partial p_i} + \frac{\partial H}{\partial p_i}\frac{\partial H}{\partial q_i} \right] \\
    &= 0
\end{align*}

As the total derivative is $0$, the Hamiltonian energy has not changed and thus, is preserved. However, numerical integration methods, e.g. the leapfrog integrator, introduce errors. Therefore, the Metropolis-Hastings step at the end of each iteration compares the Hamiltonian at the end of each iteration to the one in the beginning to ensure detailed balance, see \cref{subsubsec:hmc-integration-err}.

\textit{Volume preservation} is shown using the analogy of a vector field. If the divergence, i.e. the flow, of a vector field is $0$, then the incoming and outgoing flows keep each other in balance. Computing the divergence of $\mathbf{V} = \left( \frac{dq}{dt}, \frac{dp}{dt} \right)$:

\begin{align*}
    \operatorname{div} \mathbf{V} &= \sum_i \left[ \frac{\partial}{\partial q_i}\frac{dq_i}{dt} + \frac{\partial}{\partial p_i}\frac{dp_i}{dt} \right]  \\
    &= \sum_i \left[ \frac{\partial}{\partial q_i}\frac{\partial H}{\partial p_i} - \frac{\partial}{\partial p_i}\frac{\partial H}{\partial q_i} \right]  \\
    &= 0
\end{align*}

While volume preservation is needed for invariance and ergodicity, it also gives the freedom to not have to account for the change in volume in an acceptance step afterwards. If the volume would change, this would have to be considered in an acceptance step while computing the change would not be easy, \cite{betancourtConceptional}.

To conclude \textit{invariance} note, that detailed balance is a sufficient (but not necessary) condition for an invariant Markov Chain. As the total energy does not change during integration, the canonical distribution in \cref{eq:canonical-distribution}, depending only on $H$ up to a constant, does not change either, fulfilling detailed balance. Again, for this to hold, numerical errors need to be accounted for using a MH step.

Finally, ergodicity follows from the fact that the momentum $p$ can affect the position $q$ in arbitrary ways. Therefore, all positions can be explored and the sampler can not get stuck at some point. 


\subsection{Potential Energy}
\label{subsec:hmc-potential} 

The total energy, i.e. the Hamiltonian, is composed of a potential and a kinetic energy. While the kinetic energy can be treated as a hyperparameter and has to fit the data at hand, the potential energy is derived form the model. By definition the potential energy has to be a distribution and in fact is the one the user of HMC is interested in. 

To apply HMC sampling to a problem, the user has to derive the potential energy as the negative logarithmic to the target distribution, in analytical form, due to \cref{eq:neg-log-hamiltonian}. In the Bayesian case, the posterior distribution is often already in logarithmic form due its simplicity and can be used as it is, making the application very easy. Further, for the integration steps, the derivative of the same has to be provided. 

When using frameworks, such as \texttt{PyMC} or \texttt{Stan}, the framework itself computes the derivative using autograd modules.

A full setup and deriving all parameters is covered in \cref{sec:IMPL}.


\subsection{Integration methods}
\label{subsec:hmc-integration}

The steps HMC does are integration steps on it's trajectory in phase space. As it's not possible to use analytical integration, numerical methods are used, while introducing integration errors, by their nature. While the Metropolis-Hastings step at the end of each iteration accounts for the integration accumulated over time, the method itself must be able to integrate correctly in theory.

To motivate the use of leapfrog integration, as in \cref{alg:hmc}, it is compared to the well know Euler method. Starting from such, it's easy to derive intuition of the suited behavior of the leapfrog method and motivating the class of \textit{symplectic integrators} as a whole.

\begin{figure}
    % TODO: update integration methods to better contrast 
    \includegraphics[width=1.\linewidth]{integration_methods.png}
    \caption{Comparison of common integration techniques on a circle with $H(q,p) = \frac{q^2}{2} + \frac{p^2}{2}$. Using a steps size of $\epsilon = .3$ for each. Left to right: Euler's method, modified Euler's, leapfrog method. Euler’s method shows a drift, a common phenomenon of generic integrators. The leapfrog methods fits the circle's trajectory well. Due to the step size $\epsilon$ there are small errors as the steps size just cannot fit the tight curvature of the circle.}
    \label{fig:integration-methods}
\end{figure}

Euler's method is probably the best-known routine to approximate a system of differential equations. The update steps are computed as full updates of the variables for each step. As both updates \cref{eq:eulers-update-momentum,eq:eurlers-method-position} for step $t+\epsilon$ only depend on the position/the momentum at step $t$, they can be run in parallel and don't depend on each other. However, the independence is the reason why the method fails to trace simple shapes, e.g. a circle in \cref{fig:integration-methods}, and therefore, is not usable for more complex trajectories. Even smaller step sizes $\epsilon$ would not help and can be tested in the accompanied implementation of this work \cite{my-gitlab-repo}.

\begin{align}
    p_i(t+\epsilon) &= p_i(t) + \epsilon \frac{dp_i}{dt}(t) = p_i(t) - \epsilon \frac{\partial U}{\partial q_i}(q(t)) \label{eq:eulers-update-momentum}\\
    q_i(t+\epsilon) &= q_i(t) + \epsilon \frac{dq_i}{dt}(t) = q_i(t) + \epsilon \frac{\partial K}{\partial p_i}(p(t))  \label{eq:eurlers-method-position}
\end{align}

The pathology depicted in \cref{fig:integration-methods} for Euler’s method is called \textit{drift} and is a common one for generic integrators. Imprecisely describing the accumulated errors during integration leading to distancing of the integrated trajectory w.r.t. the region to be integrated over. The drift is problematic as it happens slowly, starting at the very beginning but slowly becoming noticeable during integration. Running Euler’s method longer would lead to a spiraling trajectory around the circle with increasing distance. In other settings the drift can be inwards leading to a halting integration, \cite{mcmcHandbook}.

A \textit{modification of Euler’s method} tries to mitigate the risk of a drift by updating the variables with respect to each other, i.e. using the currently updated variabel for the update of the next variable. \cref{eq:modified-euler-momentum,eq:modified-euler-position} show the dependent update where momentum was chosen to be updated first. The order can be interchanged. This technique preserves the volume of the region to be integrated over, while still introducing errors due to a smaller drift. The result, depicted in \cref{fig:integration-methods}, is regions where the trajectory is overshooting the shape, followed by parts of undergoing the shape. Again, reducing the step size does not fix the problem of not meeting the trajectory as depicted in orange, $\epsilon = .1$.

\begin{align}
    p_i(t+\epsilon) &= p_i(t) - \epsilon \frac{\partial U}{\partial q_i}(q(t)) \label{eq:modified-euler-momentum}\\
    q_i(t+\epsilon) &= q_i(t) + \epsilon \frac{\partial K}{\partial p_i}(p(t+\epsilon))  \label{eq:modified-euler-position}
\end{align}

Leapfrog integration uses this advantage of volume conservation due to iterative updates and applies this technique to, in HMC case, both variables. The first variable is updated for half a step $\frac{\epsilon}{2}$ (see \cref{eq:leapfrog-momentum-half}), and is used to update the second variabel for a full step (\cref{eq:leapfrog-position}) followed by another half-step update of the first variable (\cref{eq:leapfrog-momentum-full}). While both variables are updated for a full step after all, the method is able to trace the underlying trajectory much closer as seen in \cref{fig:integration-methods} on the right. Still, the trajectory does not fit exactly due to the comparable large step size, which is not able to fit the circle's curvature. Reducing $\epsilon$ far enough would increase integration time while fitting the shape even closer. However, due to the numerical nature, a perfect fit is not possible, compare \cref{fig:leapfrog-integration}. Again, the method can be tried out in the accompanying Jupyter notebooks. However, for the leapfrog method it is important to have kinetic energy independent of the position. Otherwise, the iterative update is not possible. 

\begin{align}
    p_i(t + \epsilon/2) &= p_i(t) - \frac{\epsilon}{2} \frac{\partial U}{\partial q_i}(q(t)) \label{eq:leapfrog-momentum-half}\\
    q_i(t+\epsilon) &= q_i(t) + \epsilon \frac{\partial K}{\partial p_i}(p(t+\epsilon/2))  \label{eq:leapfrog-position}\\
    p_i(t + \epsilon) &= p_i(t + \epsilon/2) - \frac{\epsilon}{2} \frac{\partial U}{\partial q_i}(q(t+\epsilon)) \label{eq:leapfrog-momentum-full}
\end{align}

\begin{figure}
    % TODO: use better graphics 
    \includegraphics[width=1.\linewidth]{leapfrog_variations.png}
    \caption{The fit of the leapfrog integration depends on the steps size $\epsilon$. While the integrator's trajectory remains stable, too large step sizes make it impossible to fit the shape (blue traj.). However, the intrinsics of the shapes are still captured, as e.g. the green trajectory would build up to a circle when running longer. Reducing the steps size enough seem to fit the shape perfectly ($\epsilon=.1$; red). Steps sizes depicted: $\epsilon \in \{ .1, 1.2, 1.8 \}$}
    \label{fig:leapfrog-integration}
\end{figure}

The leapfrog method is from the class of \textit{symplectic integrators}, which preserve the volume of the underlying space they are integrating over - matching volume preservation by the Hamiltonian - are oscillating near the trajectory over which is integrated. This makes long integration runs possible and mitigates the drift observed with the Euler methods, \cite{betancourtConceptional}.

That symplectic integrators preserve volume is easily explained when observing that the updates to the variable are shear transformations. Thus, they do not change the enclosed volume. A more rigorous explanation is provided in \cite{betancourtConceptional,mcmcHandbook}.

While symplectic integrators seem to be marvelous, they suffer from divergence in high curvature areas in the space they're integrating over. In contrast to the drift in generic integrators, the divergence happens very fast and runs towards the boundaries of phase space, \cite{betancourtConceptional}. Thus, being easy to notice.


\subsubsection{Acounting for integration errors}
\label{subsubsec:hmc-integration-err}

While symplectic integrators match the underlying structure closely, they still introduce small errors. In theory, both the Hamiltonian and leapfrog integrator are energy and volume preserving. Therefore, they'd end up with the same total energy $H(q,p)$ as they started with. However, due to the numerical errors, this is not the case and needs to be addressed. Also, symplectic iterators can diverge which also needs to be taken care of. 

To address these issues, a Metropolis-Hastings steps is added at the end of each iteration comparing the energy at the start and end. As with the random Metropolis-Hastings algorithm, it's convenient to have a symmetric distribution/Markov Chain to drop the probability of reaching one state from the other. More precisely, making the HMC reversible is needed as the integration only goes in one direction and reaching the starting position from the final position of an iteration is almost impossible. 

HMC is made reversible by just flipping the final momentum, which is valid due to the symmetry of updates and integration, and therefore pointing the HMC in the direction towards the starting position.

\begin{equation*}
    (q_L, p_L) \to (q_L, -p_L)
\end{equation*}

Having reversibility, the MH step evolves as described in \cref{eq:hmc-mh-step} \& following for $p(q_L,p_L \vert q_0, p_0)$. Due to the reversibility, the probability of reaching the final position from the starting position $\mathbb{Q}(q_0, p_0 \vert q_L, -p_L)$ and vice versa $\mathbb{Q}(q_L, -p_L \vert q_0, p_0)$ cancel each other. Yielding the well-known MH-step for symmetric chains. In the case of the HMC sampler, the MH step compares the final energy $H(q_L, -p_L)$ with the starting energy $H(q_0, p_0)$. Note, if they are the same, they cancel and $\exp(0) = 1$, i.e. always accepting the proposed position.

\begin{align}
    &= \min \left( 1, \frac{ \mathbb{Q}(q_0, p_0 \vert q_L, -p_L) \pi(q_L, -p_L) }
        { \mathbb{Q}(q_L, -p_L \vert q_0, p_0) \pi(q_0, p_0)} \right)           \label{eq:hmc-mh-step}\\
    &= \min \left( 1, \frac{\pi(q_L, -p_L)}{\pi(q_0, p_0)} \right)              \\
    &= \min \left( 1, \frac{\exp -H(q_L, -p_L)}{\exp -H(q_0, p_0)} \right)      \\
    &= \min \left( 1, \exp \lbrace -H(q_L, -p_L) + H(q_0, p_0) \rbrace \right)
\end{align}


\subsection{Benefit of avoiding local random walk}
\label{subsec:hmc-avoiding-random-walk}

To conclude this section a simple example shall demonstrate the benefit of avoiding local random walk. Assuming the target distribution to sample from is a zero-mean Gaussian with standard deviations $(1.0,  6.5, 12.0)$ on the diagonal. The kinetic energy is a standard Gaussian. The Hamiltonian therefore is:

\begin{equation*}
    H(q,p) = \frac{q' \Sigma^{-1} q}{2} + \frac{p'p}{2}.
\end{equation*}

Note the large gaps in the different variances. Those shall be used to facilitate the restricted mobility of MH in contrast to HMC. Due to the uniform steps of MH, it is expected that the radius of movement for MH is far smaller than the one of HMC. 

Both, the Hamiltonian sampler, as described in \cref{alg:hmc}, and the standard Metropolis-Hastings\footnote{The implementation for this example is also included in the accompanied GitLab repository.} implementation are run with the same configurations. For both the step size was set to $0.5$. Therefore, each step can fit inside the distribution.

HMC was configured to draw $10,000$ samples in total while taking $100$ integration steps per iteration. The samples for MH were adjusted to meet those of HMC. While HMC achieved an acceptance rate of $0.93$, MH only got to a rate of $0.54$, which meets the suggested rate of MH samplers, \cite{mcmcHandbook}. While HMC can move on the edges of the typical set by "following" the level set, MH might step off the typical set as it does not have any guiding. The samples of both are visualized in \cref{fig:3d-comparison-hmc-mh}.

\begin{figure}
    \includegraphics[width=\linewidth]{3d_comparison_scatter.png}
    \caption{Displaying the samples drawn with HMC and MH samplers in a 3D space from a zero-mean Gaussian with standard deviations $\sigma_{1,2,3} \in \{1.0, 6.5, 12.0\}$. While HMC is able to move across the space and also explore the tails of the distributions, MH fails to due so and clusters close to the center, which is where both samplers where initialized.}
    \label{fig:3d-comparison-hmc-mh}
\end{figure}

As stated in the setting of the experiment, the radius of MH is far smaller than the radius of HMC. While HMC can efficiently move fairly straight in one direction, it's hard for MH to do so as it samples at each step without taking the current direction into account. 

Further, the limitations can also be observed when viewing the trace plots of both samplers. In \cref{fig:3d-comparison-traces} the MH sampler shows a very narrow sampling behavior, compared to the distribution's standard deviations, while HMC shows its ability to utilize the available spreads in the different dimensions. Note how HMC is able to adapt its spread to the variance in the different directions as the spread in the first dimension is very small but grows rapidly in the second and third dimension.

\begin{figure}
    \includegraphics[width=\linewidth]{3d_comparison_samples.png}
    \caption{Comparing the traces of both sampling methods. While MH is not able to detect and utilize the far spread in the different dimensions, HMC is able to adapt its sampling to it and spread farther, especially in dimension three, while moving comparable narrow in dimension one.}
    \label{fig:3d-comparison-traces}
\end{figure}

However, the big advantage of HMC becomes visible when moving to far higher dimensions. While the setting is kept the same the dimensions were increased to 100 and the standard deviations are equally spread out in $[1.0, 12.0]$. To facilitate the now far higher dimensions, the step size was reduced to $0.1$ for both the algorithms. As the settings seem similar for both, the results are astonishing as the acceptance rate for HMC drops to $0.83$ and the acceptance rate for MH drops even further to almost zero! This phenomenon is depicted in \cref{fig:100d-comparison-traces} where the samples of MH collapse soon after the start into the same value and don't change anymore. For comparison, for both methods the samples in the first three dimensions are shown as in \cref{fig:3d-comparison-traces}.

\begin{figure}
    \includegraphics[width=\linewidth]{3d_comparison_samples100d.png}
    \caption{Samples of dimensions one to three in a setting of 100 dimensions. While the HMC sampler keeps a similar distribution in the first dimensions as in \cref{fig:3d-comparison-traces}, MH sampler is not able to produced acceptable samples and returns almost one value for all steps (note the different samples at the very beginning). Note, the graphics y-axis is adapted to the spread possible due to the distribution and is reduced from $[-10,10] \to [-1, 1]$.}
    \label{fig:100d-comparison-traces}
\end{figure}

Note, that while HMC performed well compared to MH, the pathology of turning around was not discussed. This happens, when HMC integrates to the borders of the typical set and the trajectory turns around to not yield a rejected sample. This, of course, should not happen as it leads to longer runtimes as it re-explores regions and also might end very close to the starting point. A solution to this offers the No-U-turn sampler, \cite{noUTurn}.
