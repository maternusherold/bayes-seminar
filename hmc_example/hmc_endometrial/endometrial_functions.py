"""Boston example specific functions.

# TODO: name what functions etc.
"""

import numpy as np


def log_posterior(theta: np.ndarray, data: np.ndarray,
                  labels: np.ndarray, **kwargs):
    """Defines the log. posterior distribution from which to sample.

    Implementation of a proportional posterior to binomial likelihood with a
    normal prior to the coefficients.

    :param theta: parameter vector
    :param data: complete data set 
    :param labels: labels
    :param kwargs: expected to have the std. dev. provided; default = 1
    :returns: evaluated log. posterior 
    """
    # sigma is assumed to be provided in this case
    if kwargs is None:
        sigma = 1
    else:
        sigma = kwargs["sigma"]

    # given by: b'X'(y-1) -> 1x1
    log_likelihood = theta.T @ data.T @ (labels - 1) \
                     - np.sum(np.log(1 + np.nan_to_num(np.exp(-1 * data @ theta), 1)))
    assert log_likelihood.shape == ()

    # given by: -1/2 log(sigma2) - b'b/(2*sigma2)
    log_priori = -.5 * np.log(sigma**2) - (theta.T @ theta) / (2 * sigma**2)
    assert log_priori.shape == ()

    return log_likelihood + log_priori


def log_posterior_grad(theta: np.ndarray, data: np.ndarray,
                       labels: np.ndarray, **kwargs):
    """Defines the gradient to a posterior with binomial likelihood and
    gaussian prior.

    :param theta: parameter vector
    :param data: complete data set
    :param labels: labels
    :param kwargs: expected to have the std. dev. provided; default = 1
    :returns: evaluated gradient of log. posterior
    """
    # sigma is assumed to be provided in this case
    if kwargs is None:
        sigma = 1
    else:
        sigma = kwargs["sigma"]

    # given by: X' [y-1 + exp(-Xb) / (1 + exp(-Xb))]
    d_log_likelihood = data.T @ (labels - 1 + np.nan_to_num(np.exp(-1 * data @ theta), nan=1)
                                 / (1 + np.nan_to_num(np.exp(-1 * data @ theta), nan=1)))
    assert d_log_likelihood.shape == theta.shape

    # given by: -1 * b / sigma2
    d_log_prior = -1 * theta / sigma**2
    assert d_log_prior.shape == theta.shape

    return (d_log_likelihood + d_log_prior) / data.shape[0]
