"""Providing utility functions for plotting """


import matplotlib.pyplot as plt
import numpy as np


fs_title = 24
fs_lable = 18


def plot_histograms(samples: np.ndarray) -> plt.figure:
    """Plots the samples as histograms grouped by chain.

    :param samples: array of chains which hold the samples
    :returns: figure object to be adjusted further if needed
    """
    chains = samples.shape[0]

    #  compute quantiles
    quantile_lower = np.quantile(samples, .05, axis=1)
    quantile_upper = np.quantile(samples, .94, axis=1)

    fig = plt.figure(figsize=(16, 16))

    ax1 = fig.add_subplot(221)
    for c in range(chains):
        ax1.hist(samples[c, :, 0], 100, range=(-10, 20), label=f"chain {c+1}", alpha=.85)
        ax1.axvline(quantile_lower[c, 0])
        ax1.axvline(quantile_upper[c, 0])
    ax1.set_title(r"$\beta_0$", fontsize=fs_title)
    ax1.legend()

    ax2 = fig.add_subplot(222)
    for c in range(chains):
        ax2.hist(samples[c, :, 1], 100, range=(-2, 2), label=f"chain {c + 1}", alpha=.85)
        ax2.axvline(quantile_lower[c, 1])
        ax2.axvline(quantile_upper[c, 1])
    ax2.set_title("PI", fontsize=fs_title)
    ax2.legend()

    ax3 = fig.add_subplot(223)
    for c in range(chains):
        ax3.hist(samples[c, :, 2], 100, range=(-10, 5), label=f"chain {c + 1}", alpha=.85)
        ax3.axvline(quantile_lower[c, 2])
        ax3.axvline(quantile_upper[c, 2])
    ax3.set_title("EH", fontsize=fs_title)
    ax3.legend()

    ax4 = fig.add_subplot(224)
    for c in range(chains):
        ax4.hist(samples[c, :, 3], 100, range=(-10, 40), label=f"chain {c + 1}", alpha=.85)
        ax4.axvline(quantile_lower[c, 3])
        ax4.axvline(quantile_upper[c, 3])
    ax4.set_title("NV", fontsize=fs_title)
    ax4.legend()

    return fig


def plot_traces(samples: np.ndarray) -> plt.figure:
    """Plot trace plots of samples per chain.

    :param samples: samples organised in chains
    :returns: figure object to be adjusted further if needed
    """
    chains = samples.shape[0]

    fig = plt.figure(figsize=(18, 12))

    ax1 = fig.add_subplot(221)
    for c in range(chains):
        ax1.plot(samples[c, :, 0], label=f"chain {c+1}")
    ax1.set_title(r"$\beta_0$", fontsize=fs_title)
    ax1.legend()

    ax2 = fig.add_subplot(222)
    for c in range(chains):
        ax2.plot(samples[c, :, 1], label=f"chain {c+1}")
    ax2.set_title(f"PI", fontsize=fs_title)
    ax2.legend()

    ax3 = fig.add_subplot(223)
    for c in range(chains):
        ax3.plot(samples[c, :, 2], label=f"chain {c+1}")
    ax3.set_title(f"EH", fontsize=fs_title)
    ax3.legend()

    ax4 = fig.add_subplot(224)
    for c in range(chains):
        ax4.plot(samples[c, :, 3], label=f"chain {c + 1}")
    ax4.set_title(f"NV", fontsize=fs_title)
    ax4.legend()

    return fig
