% =============================================================================
% Chapter deals wit the implementation of HMC. The details of what and how to 
% use, i.e. numerical stability, two methods for prob. and grad. of prob, ...
% =============================================================================

A simple Hamiltonian Monte Carlo sampling was implemented to run the examples shown in \cref{subsec:hmc-avoiding-random-walk,sec:HYPER}. While there were multiple improvements developed over time, the here described implementation refers to the initial algorithm, without any application specific improvements. The logic is exactly that described in \cref{sec:HMC}, especially in \cref{alg:hmc}, and is also available via GitLab, \cite{my-gitlab-repo}. This section will describe the setup, usage and adaption of the implementation.

\subsection{Setup}
\label{subsec:impl-setup}

Overall, the HMC algorithm is decomposed into usually three loops. The most outer loop iterates over the different chains being run. The middle loop draws the samples, i.e. exploring between the energy levels, while the most inner loop explores the typical set for that energy level. 

While the general asymptotic of MCMC samplers in \cref{eq:mcmc-asymptotic} applies, it is advisable to start several samplers with different initial positions to explore the space in a shorter time and also to utilize the multi-core architectures in a computer. Usually, the number of chains is set to the number of cores, if no other applications are run in parallel, or to the number of threads as most of todays commodity machines are equipped with hyper-threading processors. Utilizing the concept of threads, a number of chains can be run in parallel. The number of chains $C$, therefore, is a multiplier of the generated samples. Concluding, starting from the outside, the first building block of the HMC methods is a method taking the initial parameters, experiments specifications such as total number of iterations, $L$ and $\epsilon$ as well as the data. The signature of the entry methods is described in \cref{lst:hmc-entry}. The number of chains to run is defined by the dimension of the initial parameters, i.e. \texttt{init\_params} is expected to be a tensors of shape $C \times [ \text{params} ]$.  

\begin{lstlisting}[language=Python, caption={HMC entry point}, label={lst:hmc-entry}]
def run_hmc_sampler( ... ) -> [np.ndarray]:
    """Run the HMC sampling routing.

    :param init_params: initial params to start the sampling
    ...
    """
    ... 
    for t in tqdm(range(init_params.shape[0])):
        q, acceptance_ratios = run_hmc(init_params[t], ...)
    ...
\end{lstlisting}

Inside the function \texttt{run\_hmc\_sampler} the chains are started and their results are aggregated and kept per chain. Running the chains itself, compare \cref{lst:hmc-sampling}, can be done in parallel by using Python's \texttt{threadding} library, \cite{python-thredding}, by parallelising on \texttt{run\_hmc}.

The next paragraphs describes the implementation for a single chain as each thread is abstracted as a single chain. Each chain is run as a stand-alone HMC sampler, expecting just the number of steps to take as well as all the other configuration parameters, i.e. one could also skip the chain abstraction from \cref{lst:hmc-entry} and just use \cref{lst:hmc-sampling} for a single chain. Therefore, the outer method does not specify the potential energy but a function defining such energy is important from another file to be passed on to the single HMC steps. 

\begin{lstlisting}[language=Python, caption={HMC samppling loop}, label={lst:hmc-sampling}]
from energy_func import log_posterior, log_posterior_grad
...
def run_hmc( ... ) -> [np.ndarray]:
    """Run HMC sampler as one chain implementation.
    
    ...
    """
    ... 
    for idx in tqdm(range(total_iterations)):
        q, acceptance_prob = hmc_step( ... )
    ...
\end{lstlisting}

Per iteration, that is one sampling step, the HMC method actually takes several steps. As each of these exploration phases per energy level are complex themselves, those are extracted into an extra method respecting high coherence and low coupling of functionality. The signature of a single HMC step is described in \cref{lst:hmc-step}. Important to note here, that the function expects dedicated callable object computing the logarithmic posterior and its derivative, i.e. $U(q),\ \partial U(q)$, which are application dependent. The steps taken within \texttt{hmc\_step} are exactly those described in \cref{alg:hmc}.

\begin{lstlisting}[language=Python, caption={HMC step implementation}, label={lst:hmc-step}]
def hmc_step(log_posterior: callable, 
    log_posterior_gradient: callable,
    ...) -> Tuple(np.ndarray, np.ndarray):
    """Basic step of the HMC algorithm running leapfrog integration for set steps.

    :param log_posterior: function evaluating the log posterior
    :param log_posterior_gradient: function evaluating the gradient of log posterior
    ... 
    """
    ... 
\end{lstlisting}


\subsection{Utility functions included}
\label{subsec:impl-utility}

Besides the pure sampling functions some notable utilities included are the evaluation of the \textit{Gelman-Rubin} metric, i.e. the \textit{potential scale reduction factor (PSRF)}, \cref{lst:utils-gelman-rubin}, and an evaluation of relevant quantiles, \cref{lst:utils-quantiles}. 

The scale reduction factor, as described by \cite{PSRF}, is a multivariate generalisation of the univariate case initially described by \cite{gelman-rubin-measure} and elaborated as sampling statistic in \cite{GelmanBayesianDA}. While one can observe the convergence easily via the trace plot, the metric by Gelman et al. indicates, if the chains can be expected to converge, when running the sampler longer. 

\begin{lstlisting}[language=Python, caption={Gelman-Rubin convergence metric, \cite{PSRF}}, label={lst:utils-gelman-rubin}]
    def gelman_rubin(samples: np.ndarray) -> float:
        """Computes the Gelman-Rubin diagnostic measure. 
        ... 
        """
\end{lstlisting}

The assumption behind the PSRF is that the same parameter across multiple chains should reduce in variance as it converges. Therefore, the PSRF compares the within variance $W(\theta)$ and between variance $B(\theta)$ for one parameter across chains $C$. It then compares both to assess if convergence has happened or can be expected. The metric assumes a random initialization of all chains. While $W(\theta)$ will underestimate the variance during the first samples, using the between variance $B(\theta)$ is important. The asymptotic of the metric follows the law of large numbers and states the convergence of both, the within and between variance, to the true variance of the parameter for $n \to \infty$. The computation of the PSRF, as implemented, is stated in \cref{eq:PSRF}, where $n$ describes the samples obtained per chain, $s_i$ the within chain variance of $\theta$ and $\bar{\theta}_j,\ \bar{\bar{\theta}}$ the within-chain mean and across-chain mean, respectively.

\begin{align}
    W &= \frac{1}{C} \sum^C_{i=0} s^2_i                     \\
    B &= \frac{n}{C-1} \sum^C_{j=0} (\bar{\theta}_j - \bar{\bar{\theta}})^2 \\
    \hat{Var}(\theta) &= \frac{n-1}{1}W + \frac{1}{n}B      \\
    \hat{R} &= \sqrt{\frac{\hat{Var}(\theta)}{W}} \label{eq:PSRF}
\end{align}

As a rule of thumb $\hat{R} \leq 1.1$ is anticipated for convergent chains. 

Further, for a quick comparison of distributions the evaluation of quantiles is helpful and provided with \cref{lst:utils-quantiles}. 

\begin{lstlisting}[language=Python, caption={Evaluation of relevant quantiles per chain}, label={lst:utils-quantiles}]
def quantiles_per_chain(samples: np.ndarray, burnin: int = 0,
                    quantiles: np.ndarray = [.025, .25, .5, .75, .975]):
    """Compute samples quantiles per chain.
    ... 
    """
\end{lstlisting}
