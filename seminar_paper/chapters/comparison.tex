% =============================================================================
% In the comparison the use of HMC is explained for a regression and 
% classification context. While a short comparison with the MH sampler is given
% by the case of endometrial cancer, further comparisons are done with larger 
% data sets as e.g. the alcohol and boston data set.
% =============================================================================

Lastly, the applicability of HMC is demonstrated for the cases of linear regression and linear classification, both in the Bayesian context. As a reference, the results obtained with the Metropolis-Hastings variant of MCMC are stated. While the focus is to demonstrate the usage of HMC for the two problem classes, the superiority of HMC w.r.t. MH should become clear while also showcasing HMC's drawbacks. 

However, models presented don't claim to be the best fit for the problem as fitting a model itself is out of scope. How results, obtained from the posterior distribution, can be used to improve a model are covered, e.g., in \cite{GelmanBayesianDA}.


\subsection{HMC for Classification}
\label{subsec:comp-class}

Linear classification models, such as the linear discriminant analysis or the Na\"ive Bayes model, are well known and provide well-interpretable results due to the linear relationship between the variables, which can be transformed using basis functions to yield powerful variants, \cite{patternRecognition}. Here, a logistic regression is used. 

For the first example the data set on endometrial cancer by \cite{endometrial-data} is used and analysed according to \cite{endometrial-book}. Prior to usage, the variables where centered around zero and standardized, if continuous. The variable's distributions are depicted in \cref{fig:endometrial-data}. The target variables are binary, $y \in \{0,1\}$, leading to the Sigmoid as link function.

As coefficient prior a zero-mean Gaussian is used, according to the general assumptions in linear regression, with a relatively large variance of $10$ per variable, \cref{eq:endometrial-coeff-prior}. This was done as no further information about the covariance of the coefficients is known and thus a \textit{uninformative prior} was used. 

\begin{equation}
    \label{eq:endometrial-coeff-prior}
    \beta \sim \mathcal{N}(0,\sigma^2_{\beta}I), \quad \sigma^2 = 10
\end{equation}

Due to the binary targets a Binomial distribution set as target distribution and is used to derive the target's likelihood, \cref{eq:endometrial-target-dist}. To define the posterior distribution the fact of proportionality and monotonicity was used to skip the evidence term and reads as in \cref{eq:endometrial-poterior}. 

\begin{align}
    f(y \vert X,\beta) &= \prod p^{y_i} (1−p)^{1−y_i} \label{eq:endometrial-target-dist} \\
    \log p(\beta \vert \cdot) &\propto \log p(y \vert \cdot) + \log p(\beta \vert \cdot)    \label{eq:endometrial-poterior}
\end{align}

\begin{figure}
    \includegraphics[width=1.\linewidth]{endometrial_data.png}
    \caption{The endometrial cancer data sets facilitates three covariates and one binary target. The three variables where centered around zero while the continuous ones were standardized as well. Without further ado, a weak dependence between the categorical $NV$ and the target is visible.}
    \label{fig:endometrial-data}
\end{figure}

As the results from using the MH sampler are known from \cite{endometrial-book}, the obtained quantiles from $10 * 10^6$ samples are listed in \cref{tab:endometrial-mh-quantiles}.

\begin{table}[]
    \begin{tabular}{r|rrrrr}
            & 2.5\%     & 25\%      & 50\%      & 75\%  & 97.5\%    \\ \hline
        $\beta_0$  & -0.345  & 1.268  & 2.721     & 4.683 & 9.335     \\
        PI  & -1.413    & -0.766    & -0.455    & -0.158    &  0.366    \\
        EH  & -3.402    & -2.515    & -2.100    & -1.722    & -1.082  \\
        NV  & 2.107     & 5.229     & 8.126     & 12.041    & 21.319
    \end{tabular}
    \caption{Known quantiles from MH sampling with $10E6$ steps on the data presented in \cref{fig:endometrial-data}. According to \cite{endometrial-book} the sampling ran for $2.58$min. on a multi-core setup.}
    \label{tab:endometrial-mh-quantiles}
\end{table}

For the HMC sampler, the posterior as in \cref{eq:endometrial-poterior} and it's gradient were specified and passed to the sampler function as described in \cref{lst:hmc-sampling}. The sampler was run for $10 * 10^4$ iterations with $L=10$ steps and a step size of $\epsilon=0.1$. In total, the HMC method only makes an order of magnitude less steps, i.e. $10 * 10^5$ vs. $10 * 10^6$.  

The samples were drawn by four chains. As initializations for the postion the following were used: all ones vector, draws from a uniform distribution, draws from a standard normal and draws from a standard normal with variance $\sigma=10$ were used. 

\begin{align}
    \beta^{(1)} &= (0,0,0,0)'                   \\
    \beta^{(2)} &\sim \mathcal{U}[0,1]^4        \\
    \beta^{(3)} &\sim \mathcal{N}(0, I_4)       \\
    \beta^{(4)} &\sim \mathcal{N}(0, 10*I_4)    
\end{align}

The runtime of the vanilla HMC implementation took $2.30$min on average per chain producing ten thousand samples each. 

After running the sampler, a first observation to make is the convergence as well as the sample density, i.e. the amount of accepted proposals. Concerning the convergence, the trace plots per parameter, per chain are conclusive. Observing the traces in \cref{fig:endometrial-traces}, the chains have converged for the parameters \texttt{PI} and \texttt{EH} while the coefficients for the intercept and \texttt{NV} show a lot of variance. Computing the PSRF confirm this observation, \cref{tab:endometrial-psrf}.
The acceptance probabilities are tracked by the sampling procedure and are close to $55\%$ for all chains. As this is below the suggested value of $65\%$ a solution could be to decrease the steps size $\epsilon$ or decreasing the amount of steps to take per iteration. Note, the initial setting followed a possible start configuration of $\epsilon * L = 1.0$, as described in \cref{subsec:hyper-step-size}. Going further, one would now adjust those and might even start several chains with different configurations for faster exploration of such settings. 

\begin{table}
    \begin{tabular}{r|rrrr}
        & $\beta_{1}$ & $\beta_{2}$ & $\beta_{3}$ & $\beta_{4}$ \\ \hline
        $\hat{R}$ & 1.2100 & 1.0025 & 1.0105 & 1.2188  \\
    \end{tabular}
    \caption{The PSRF per HMC sampled parameters per parameter across chains. While the parameters for \texttt{PI} and \texttt{EH} show good convergence with values $\sim 1.1$, the coefficients for the intercept and \texttt{NV} have not converged, yet.}
    \label{tab:endometrial-psrf}
\end{table}

\begin{figure}
    \includegraphics[width=1.\linewidth]{endometrial_traces.png}
    \caption{Trace plots indicate if a chain has converged for a specific parameter. In this case, the chains for \texttt{PI} and \texttt{EH} show good convergence while the chains for the other parameters have not converged.}
    \label{fig:endometrial-traces}
\end{figure}

An observation already possible to see in \cref{fig:endometrial-traces} is the similarity of chains and therefore the samples obtained by the chains with standard normal initial values. Plotting the distribution of samples showed almost congruent curves. Therefore, the fourth chain was dropped when analyzing the distributions and their $98\%$ credibility intervals, shown in \cref{fig:endometrial-distributions}.

\begin{figure}
    \includegraphics[width=1.\linewidth]{endometrial_distributions.png}
    \caption{The distributions show a high similarity between chains for the converged parameters hinting a good results for those. For the other two, it is interesting to observe a bi-modal posterior for the first chain. The two modes are covered by chain 2 and chain 3, each. In blue, the $89\%$ credibility intervals are drawn per chain. Note, due to high similarity with chain 3 the distributions resulting from chain 4 were dropped.}
    \label{fig:endometrial-distributions}
\end{figure}

From both sampling methods, the modes are taken as the estimated parameters, i.e. the $\theta_{MAP}$, to evaluate the model itself and indicate the exploration capability of the model. For all five models, the one sampled with Metropolis-Hastings and the four sampled with HMC, the binary cross-entropy on the real data is computed as a goodness of fit for describing the data. Note, the aim is not to fit the best possible model but to evaluate the sampler's capability to explore the target distributions. When developing a model for further use a proper split into training and validation data is needed! 
The results are shown in \cref{tab:endometrial-classification-all} and a detailed comparison of $\beta^{(4)}$ against MH in \cref{fig:endometrial-classification}. Clearly, the parameters obtained with HMC outperform the model obtained with MH sampling.

\begin{table}
    \begin{tabular}{r|rrrrr}
         & MH & $\beta^{(1)}$ & $\beta^{(2)}$ & $\beta^{(3)}$ & $\beta^{(4)}$ \\ \hline
        BCE & $0.366$ & $0.307$ & $0.313$ & $0.309$ & $0.308$  
    \end{tabular}
    \caption{Results of classifying the training data for all methods, the Metropolis-Hastings and the Hamiltonian Monte Carlo parameters. The error is measured by the binary cross-entropy loss and noted in the title of each set of parameters. Clearly, the parameters sampled with the HMC method outperform those obtained with the MH sampler while needing less iterations.}
    \label{tab:endometrial-classification-all}
\end{table}

\begin{figure}
    \includegraphics[width=1.\linewidth]{endometrial_classification.png}
    \caption{Comparison of the best preforming parameter set, w.r.t. BCE, obtained with HMC compared to the such obtained with MH.}
    \label{fig:endometrial-classification}
\end{figure}

The simple example already shows the efficiency of HMC in exploring the target space, by yielding better results as a simple MH sampler while needing only $\frac{1}{10}$ of samples, as well as the applicability in general. It also shows the dependence on a suitable setting of the hyperparameters, as elaborated in \cref{subsec:hmc-avoiding-random-walk}.


\subsection{HMC for Regression}
\label{subsec:comp-regression}

The approach for applying HMC to a regression model stays the same as described in \cref{subsec:comp-class}, only the link function connecting from the logits needs to be applied. The final comparison, using HMC in a regression settings facilitates the efficiency in high dimensions, using a data set of student's alkohol consumption, taken form \cite{alc-school}. Further, this time the model includes another prior for the variance. Compare \cref{eq:endometrial-target-dist} where just one parameter was needed to describe the target distribution. Including several priors makes its harder to factorize a posterior and sampling is a convenient method to still be able to analyse it.

Overall, the data sets includes $33$ features, including the later target grades. As several of such features are categorical, those are split up into dummy variables yielding a total of $46$ features on $395$ observations. A detailed list of variables is appended in \cref{tab:students-variable-desc}. Further the data was standardized to mitigate scale dependencies.

In this example the final math grade is used as target. It's distribution is depicted in \cref{fig:students-grade-dist}, hinting a normal distribution, as usual for grades. However, the amount of failed tests hurt this assumption and was therefore dropped. 

\begin{figure}
    \includegraphics[width=1.\linewidth]{students_grade_distr.png}
    \caption{The student's grades distribution in their final math test. A normal kurve with the empirical statistics of the data set is shown in orange. While the assumption of a normal holds, the deviations most likely come from the failed tests. See \cref{fig:students-grade-dist-est} where the failed test were removed.}
    \label{fig:students-grade-dist}
\end{figure}

The data set includes the students first and second period grades as well but which are dropped in this example. A variable for the intercept is added, yielding a total of $45$ covariates to investigate, i.e. sampling form a $45$ dimensional space. 

Due to the depicted target distribution in \cref{fig:students-grade-dist} a typical regression model with $y \sim \mathcal{N}(X\beta, \sigma^2I)$ is assumed. The coefficients are normally distributed as well, again using a non-informative prior as the variance is scaled with $10$. The variance parameter for the target distribution is assumed to be inverse gamma distributed with both $\alpha,\beta = 1$. The parameter vector to estimate is the given in \cref{eq:students-param-vec}. 

\begin{equation}
    \label{eq:students-param-vec}
    \theta = (\beta_0, \beta_1, \dots, \beta_{45}, \sigma)'
\end{equation}

The log posterior and its derivative are derived as follows. Again, those functions are provided to the algorithm as callable functions. 

\begin{align*}
    \log p(\beta, \sigma^2 \vert \cdot) \propto &\log p(y \vert \cdot) \\ &+ \log p(\beta \vert \cdot) + \log p(\sigma^2 \vert \cdot)  \\
    \nabla_{\beta, \sigma^2} \log p(\beta, \sigma^2 \vert \cdot) \propto ( &\nabla_{\beta} \log p(\beta, \sigma^2 \vert \cdot), \\ &\nabla_{\sigma^2} \log p(\beta, \sigma^2 \vert \cdot)) 
\end{align*}

The posterior distributions of all $45$ coefficients were explored with the HMC, drawing again $10 * 10^4$ samples with a step size $\epsilon=0.001$ and number of steps $L=10$. Two chains with initial values of all-zero and std. normal draws were run yielding acceptance probabilities of $82.5\%$ and $78.0\%$, respectively. Note, the steps size is considerably lower as in the first examples \cref{subsec:comp-class} and thus the trajectories are considerably shorter. This is needed to still obtain a fairly high acceptance rate, compare the results from \cref{subsec:hmc-avoiding-random-walk} with $100$ dimensions. 

Due to the large number of variables, the trace and distribution plots are not listed but available in the accompanying repository. However, the PSRFs per estimation parameter are listed in \cref{tab:students-psrf} and coincide what can be observed from the trace plots. Only $12$ of the in total $45$ distributions over the estimation parameters have converged, as their $\hat{R} \sim 1.1$. This can also be observed when viewing the distributions per parameter as such are wide spread and differ between chains. This observation hints that $10 * 10^4$ samples are not enough in $45$ dimensions and far more should be drawn. This makes the case for efficient sampling as mentioned in the introductory examples \cref{fig:high-dim-samples} and the problem of the curse of high dimensionality in general. Gladly, each chain sampling ten thousand samples only took about $23$min. As the sampling times evolves nearly linear - keeping the other parameters as is - with the amount of samples to draw on reasonable architectures, sampling an order of magnitude more results could be done in just four hours. As the acceptance probabilities are quite high, it can be assumed that sampling longer also yields more diverse samples, i.e. a better exploration of the typical set. 

Finally, for the converged distributions, the parameters are taken to estimate the target distribution. Such is plotted in \cref{fig:students-grade-dist-est} and while the estimated parameters form the first chain show a shift in the mean, the second chain found a good estimate for the mean. Still, both chains overestimate the variance as both distributions have to much volumes in the tails. 

\begin{figure}
    \includegraphics[width=1.\linewidth]{students_grade_distr_est_normal_passed.png}   
    \caption{The empirical distribution for the target variable, the final math grade (standardized), accompanied by the true target distribution, estimated posterior from the first and second chain that were run, respectively. The parameters were taken form the sampling procedures. Clearly the distribution of the first chain deviates from the true posterior while the second chain fit the true distribution quite well. However, the variance was clearly overestimated.}
    \label{fig:students-grade-dist-est}
\end{figure}

Due to the better fit, the converged coefficients of the second chain are used further. A graphic analysis shows that the model is only able to capture the target's distribution, i.e. a normal, but not the structure of it, see \cref{fig:students_reconstruction}. However, the coefficients were be analysed and to hint on the advantages of Bayes regression. As we now have a distribution over the estimated coefficients, we can construct credibility intervals. In the case of $\beta_{1}$ the coefficient lies with a probability of $95.0\%$ within the interval of $[-.049,.443]$. For the other coefficients see \cref{tab:students_credibility_inter}. The important observation to make here is that the model is uncertain about the effect the variable has. It can have a negative, but also a positive effect. Similar observations can be made with other coefficients in the table. 

\begin{table}
    \begin{tabular}{r|ccc}
         & $2.5\%$   & $50.0\%$   & $97.5\%$  \\ \hline
        $\beta_{1}$  & -4.900e-02 & 1.650e-01  & 4.430e-01    \\
        $\beta_{5}$  & -3.700e-01 & -1.210e-01 &  3.420e-01    \\
        $\beta_{8}$  & -6.450e-01 & -3.790e-01 & -1.220e-01    \\
        $\beta_{10}$ &  2.300e-02 &  3.370e-01 &  5.130e-01    \\
        $\beta_{12}$ & -2.100e-01 & -9.400e-02 &  3.780e-01    \\
        $\beta_{13}$ & -3.100e-02 & -7.000e-03 &  1.900e-02    \\
        $\beta_{22}$ &  2.920e-01 &  5.500e-01 &  8.660e-01    \\
        $\beta_{24}$ &  1.240e-01 &  9.690e-01 &  1.269e+00    \\
        $\beta_{37}$ &  2.760e-01 &  4.350e-01 &  5.310e-01    \\
        $\beta_{39}$ &  4.420e-01 &  7.580e-01 &  1.033e+00
    \end{tabular}
    \caption{Table showing the converged coefficients and their quantiles, i.e. the $95.0\%$ credibility intervals.}
    \label{tab:students_credibility_inter}
\end{table}


\begin{figure}
    \includegraphics[width=1.\linewidth]{students_grade_distr_est_interval.png}
    \caption{While the model is able to represent the distribution, it is not capable of representing the target's structure as the reconstruction is far off. The green shaded area is the interval of one standard deviation, underlining the models uncertainty.}
    \label{fig:students_reconstruction}
\end{figure}

Concluding the example on linear regression, the need for an efficient sampler got obvious as the distributions are now in high dimensions and covering such, a lot of samples are required. By using a sampler with the capability of high acceptance rates and efficient traversal to compute those samples, this is possible in manageable time. 


\subsection{Full Bayesian Approach}
\label{subsec:comp-full}

Usually, a regression model is fit to explain the data but also to predict the target value for a new set of features, e.g. estimating the price of a house. The great advantage one has with a sampling approach as described here, is to have access to the predictive distribution. This distribution is used to regress on the new features, using the fitted model. The overall approach is called \textit{full Bayesian approach}.

Assuming a model was fit to the data, one would derive the prediction as the posterior of the new target, given the new features and the old data. The equation derives as follows:

\begin{equation}
    p(\hat{y} \vert x_{new}, X) = \int p(\hat{y} \vert x_{new}, \theta)p(\theta \vert X) d\theta
\end{equation}

Therefore, instead of just receiving a single estimate for a new set of features, one is able to estimate a distribution per sample.

\begin{table}
    \tiny
    \begin{tabular}{r|l}
         & description \\ \hline
         school & binary: 'GP', 'MS' \\ 
         sex & binary: 'F', 'M' \\ 
         age & numeric: from 15 to 22 \\ 
        address & binary: 'U' - urban or 'R' - rural \\ 
        famsize & binary: 'LE3' - $\leq3$ or 'GT3' - $>3$) \\ 
        Pstatus & parent's cohabitation status; binary \\ 
        Medu & mother's education; numeric: 0 - 4 \\ 
        Fedu & father's education; numeric: 0 - 4 \\ 
        Mjob & mother's job; nominal \\ 
        Fjob & father's job; nominal \\ 
        reason & reason for this school; nominal \\ 
        guardian & nominal: 'mother', 'father' or 'other' \\ 
        traveltime & numeric (hours) \\ 
        studytime & weekly study time; numeric (hours) \\ 
        failures & number of class failures; numeric \\ 
        schoolsup & extra educational support; binary \\ 
        famsup & family educational support; binary \\ 
        paid & extra paid classes; binary \\ 
        activities & binary \\ 
        nursery & attended nursery school; binary \\ 
        higher & plan for higher education; binary \\ 
        internet & internet access at home; binary \\ 
        romantic & with a romantic relationship; binary \\ 
        famrel & quality of family; numeric: 1-5 \\ 
        freetime & free time; numeric: 1-5 \\ 
        goout & going out; numeric: 1-5 \\ 
        Dalc & workday alcohol consump.; numeric: 1-5 \\ 
        Walc & weekend alcohol consump.; numeric: 1-5 \\ 
        health & current health status; numeric: 1-5 \\ 
        absences & number absences; numeric: 0-93 \\ 
        G1 & math grade, first period \\ 
        G2 & math grade, second period \\ 
        G3 & math grade, final test \\ 
    \end{tabular}
    \caption{Variable names and their description in the student's alcohol consumption data set.}
    \label{tab:students-variable-desc}
\end{table}

\begin{table}
    \centering
    \tiny
    \begin{tabular}{r|llr|l}
                    & $\hat{R}$ & &                 & $\hat{R}$ \\ \hline
        $\beta_0$   & 2.4085    & & $\beta_{23}$    & 19.9042 \\ 
        $\beta_1$ & 1.1355      & & $\beta_{24}$ & 1.2122  \\ 
        $\beta_2$ & 1.4708      & & $\beta_{25}$ & 28.9584  \\ 
        $\beta_3$ & 3.2385      & & $\beta_{26}$ & 3.3853 \\ 
        $\beta_4$ & 1.5957      & & $\beta_{27}$ & 3.3747 \\ 
        $\beta_5$ & 1.2119      & & $\beta_{28}$ & 6.4889 \\ 
        $\beta_6$ & 4.4681      & & $\beta_{29}$ & 8.3687 \\ 
        $\beta_7$ & 3.1608      & & $\beta_{30}$ & 10.1569 \\ 
        $\beta_8$ & 1.0947      & & $\beta_{31}$ & 1.5499  \\ 
        $\beta_9$ & 1.7347      & & $\beta_{32}$ & 2.3492  \\ 
        $\beta_{10}$ & 1.1708   & & $\beta_{33}$ & 1.3544  \\ 
        $\beta_{11}$ & 1.8879   & & $\beta_{34}$ & 4.9542  \\ 
        $\beta_{12}$ & 1.2471   & & $\beta_{35}$ & 7.4957  \\ 
        $\beta_{13}$ & 1.1340   & & $\beta_{36}$ & 8.5327  \\ 
        $\beta_{14}$ & 7.4680   & & $\beta_{37}$ & 1.0426  \\ 
        $\beta_{15}$ & 2.9039   & & $\beta_{38}$ & 9.2444  \\ 
        $\beta_{16}$ & 8.3813   & & $\beta_{39}$ & 1.0842  \\ 
        $\beta_{17}$ & 4.7166   & & $\beta_{40}$ & 6.6290  \\ 
        $\beta_{18}$ & 8.6949   & & $\beta_{41}$ & 2.1911  \\ 
        $\beta_{19}$ & 5.5191   & & $\beta_{42}$ & 4.3798  \\ 
        $\beta_{20}$ & 7.2819   & & $\beta_{43}$ & 2.0609  \\ 
        $\beta_{21}$ & 18.921   & & $\sigma^2$ & 1.2278 \\ 
        $\beta_{22}$ & 1.1572   & & & \\ 
    \end{tabular}
    \caption{The PSRF for all coefficients from the students model described in \cref{subsec:comp-regression}. Only $11$ of the $45$ samplers have converged, w.r.t. $\hat{R} \leq 1.3$. The example was run with only two chains with an all-zero and an std. normal initialization.}
    \label{tab:students-psrf}
\end{table}
