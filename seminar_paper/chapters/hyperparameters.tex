% =============================================================================
% Chapter describes the different hyperparameters to be set in the HMC 
% algorithm. Those include the kinetic energy, steps size and integration 
% length. =============================================================================

There are several hyperparameters to set when using the HMC sampler. This helps to improve efficiency but can also lead to breaking the sampler's assumptions, leading to rejecting almost all proposals and not gaining any information. Taking the 100 dimensional example from \cref{subsec:hmc-avoiding-random-walk} and setting the step size $\epsilon = 1.0$, matching the smallest standard deviation, leads to rejecting all proposals and not sampling any data point. In this section the different hyperparameters are described and their behavior is discussed to mitigate such failures. 

\subsection{Step size and integration steps}
\label{subsec:hyper-step-size}

The above example shows how important the integrator's step size is. It is important to evaluate the step size $\epsilon$ together with the amount of steps to take $L$. While a very small step size, with respect to the sampling space, coupled with the right amount of steps leads to a fine acceptance rate, it is on the cost of efficiency as the sampler could probaby move faster. That is larger step size and less steps in total. In contrast, too many steps with a descent step size also lead to inefficiency as the sampler turns around on it trajectory as it might leave the typical set. In this case, as mentioned in \cref{subsec:hmc-avoiding-random-walk} as well, the amount of steps should've been decreased. \cref{fig:trajectory-u-turn} shows this pathology. Note, decreasing the step size would not have brought the desired improvement as the sampler itself stays well in the target set. 

\begin{figure}
    \includegraphics[width=\linewidth]{returning_trajectory.jpg}
    \caption{The trace of a HMC sampler on a bivariate normal distribution with a strong correlation between the two dimensions. Clearly, the amount of steps to take per iteration is set to high as the trajectory shows to turn around when reaching a point in the upper right distribution. This is describes as U-turn and is tackled by \cite{noUTurn}. This figure was taken form \cite{mcmcHandbook}.}
    \label{fig:trajectory-u-turn}
\end{figure}

Another unwanted result in setting wrong pairs of steps size and steps to take is that the sampler simply does not explore the target distribution. To elaborate this case, again the setting from \cref{subsec:hmc-avoiding-random-walk} is used, while switching to the three dimensional case with standard deviations $\{1.0, 6.5, 12.0\}$. The samples drawn from two HMC samplers with different step sizes, $\epsilon_{1}=0.5,\ \epsilon_{2}=0.001$, are shown in \cref{fig:hmc-step-size-comp-2D,fig:hmc-step-size-comp-3D}. It is well observable that the sampler with smaller step size is not able to explore the phase space. Further, \cref{fig:hmc-step-size-comp-3D} highlights the now very tightly connected samples, depicted as wringing.

\begin{figure}
    \includegraphics[width=\linewidth]{hmc_step_size_comparison_2D.png}
    \caption{The HMC is not able to explore the target distribution when the step size is set incorrectly. In this case the amount of steps were kept the same but the step size was reduced from a suitable $\epsilon_{1}=0.5$ to $\epsilon_{2}=0.001$. The sampled points show the inability of exploring the space in directions of greater variance, i.e. dimension two and three.}
    \label{fig:hmc-step-size-comp-2D}
\end{figure}

\begin{figure}
    \includegraphics[width=\linewidth]{hmc_step_size_comparison3D.png}
    \caption{Observing the sampled points gives a clearer picture how the HMC sampler tried to explore the space by wringing around the true mean but not being able to escape. In comparison, the sampler with larger step size was able to fill the target space and travel greater distances, utilizing the variances in all three dimensions.}
    \label{fig:hmc-step-size-comp-3D}
\end{figure}

Even increasing the samples drawn by a factor of $100$ did not yield a comparable cover of the space, highlighting the importance of step size and amount of steps for the algorithm's efficiency. Comparison of coverage is shown in \cref{fig:hmc-step-size-comp-fac100}.

\begin{figure}
    \includegraphics[width=\linewidth]{hmc_step_size_comparison_factor100.png}
    \caption{As the amount of samples to be drawn by a factor of 100 the samples now spread across the same distance and those with the initial settings from\cref{subsec:hmc-avoiding-random-walk}. For the second sampler, only every hundredth sample is display to yield a comparable setting with the first sampler.}
    \label{fig:hmc-step-size-comp-fac100}
\end{figure}

While setting the hyperparameters correctly is often done in a trial-and-error fashion, a good point to start is by picking a step size smaller than the smallest eigenvalue, corresponding to the variance of the smallest principal competent. This ensures the samplers ability to stay within the typical set while still exploring this dimension. Another starting point is given by \cite{mcmcHandbook} to pick both parameters to yield $\epsilon * L = 1.0$. A fairly automated procedure is to start with a small step size and increase such to eliminate autocorrelation in the samples \cite{betancourtConceptional}. While doing so, the amount of steps can be varied as well to travel more efficiently.


\subsection{Choices of kinetic energy}
\label{subsec:hyper-kinetic}

Another important set of hyperparameters are kinetic energies. Those, as described in \cref{subsec:hmc-sampler} are needed to explore phase space between the energy levels. And as the efficiency in this exploration is highly depended on the similarity between the true energy distribution and the conditional energy distribution, the kinetic energy plays an important role for the overall efficiency. Further, a proper chosen kinetic energy can reduce the risk of an breaking out integrator as described in \cref{subsec:hmc-integration}. However, it's important to then choose an appropriate integrator when, e.g. introducing a dependence of momentum $p$ on position $q$.

The most commonly used kinetic energies are Gaussians. The default kinetic energy, Euclidean-Gaussian energy \cref{eq:kinetic-energy-euclidean}, for leapfrog integrators does not consider a dependent momentum. 

\begin{equation}
    \label{eq:kinetic-energy-euclidean}
    \pi(p \vert q) = \pi(p) = \mathcal{N}(p \vert 0, M)
\end{equation}

The Riemannian kinetic energy (\cref{eq:kinetic-energy-riemann}), an adoption uses the covariance of the position $q$ to adapt to the local curvature in space, leading to a more smooth transitioning and, if tuned correctly, less break out of the integrator, \cite{betancourtConceptional}. While considered and advancement, in general the kinetic energy was defined to be depended on the position in the initial Hamiltonian equations, \cref{eq:hamiltonian}.

\begin{equation}
    \label{eq:kinetic-energy-riemann}
    \pi(p \vert q) = \mathcal{N}(p \vert 0, \Sigma(q))
\end{equation}

While there are also non-Gaussian kinetic energies proposed, they have empirically shown to not work well in practice. A thorough proof to this is still open, \cite{betancourtConceptional} but can be explained by using the central limit theorem. Without further assumptions, the energy distribution will follow a Gaussian in high dimensions. Therefore, it makes sense to also approximate it by such. 
