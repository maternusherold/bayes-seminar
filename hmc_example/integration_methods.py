"""
Defining several discretized integration methods.

The HMC algorithm builds on a discretized integration method traversing the
phase space on an energy level.
Common methods are Euler's method and a modification of it as well as the
Leapfrog algorithm.
"""


# TODO: implement Leapfrog algorithm as class

import numpy as np


def derivative_std_normal_kinetic_energy(p):
    """Computes the derivative of the kinetic energy from a std. normal
    energy, i.e. mu = 0, sigma = diag(1, ..., 1)
        dq/dt = dH(q,p)/dp = dK(q,p)/dp = d(p' * M^-1 * p * 1/2)/dp = M^-1*p = p

    :param p: current momentum
    :returns: derivative of the kinetic energy w.r.t. p
    """
    return p


def std_normal_kinetic_energy(p):
    """Computes the kinetic energy of the std. normal. """
    return p.T.dot(p) / 2


def compute_leapfrog(p, q, **kwargs):
    """Computes update of Euler's method for integral approximation. """
    # TODO: adapt function to receive grad of U
    # TODO: ask how to adapt
    pt = p - (kwargs['eps'] / 2) * (q - 5)
    qt = q + kwargs['eps'] * (pt / 1)
    pt = pt - (kwargs['eps'] / 2) * (qt - 5)
    return qt, pt


def run_leapfrog_method(init_position=0, init_momentum=1, dim=2,
                        steps=60, step_size=.1):
    """Runs Leapfrog method for a defined number of steps. """
    # TODO: get dim from HMC directly
    acc = np.zeros((steps, 2, dim))
    q, p = init_position, init_momentum  # init params
    for i in range(steps):
        q, p = compute_leapfrog(p=p, q=q, eps=step_size)
        acc[i] = [q, p]
    return acc, q, p
