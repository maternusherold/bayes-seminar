"""General implementation of a Hamiltonian Monte Carlo sampling step.

By expecting all variables as input the implementation is flexible to
be applicable to all sampling case done with the standard HMC step.
"""

import numpy as np
from scipy.stats import multivariate_normal as mvnormal
from scipy.stats import uniform


def hmc_step(log_posterior: callable, log_posterior_gradient: callable,
             data: np.ndarray, labels: np.ndarray,
             mass_matrix: np.ndarray, current_position: np.ndarray,
             epsilon: float = .01, step_count: int = 10):
    """Basic step of the HMC algorithm running leapfrog integration for set steps.

    :param log_posterior: function evaluating the log posterior
    :param log_posterior_gradient: function evaluating the gradient of log posterior
    :param data: input data
    :param labels: labels
    :param mass_matrix: mass matrix for kinetic energy
    :param current_position: position computed by a prev. step
    :param epsilon: step size in leapfrog iteration
    :param step_count: number of leapfrog steps
    :returns: sampled position and acceptance probability
    """

    mass_matrix_inv = np.linalg.inv(mass_matrix)  # needed later for update
    dimensions = current_position.shape[0]
    momentum = mvnormal.rvs(mean=np.zeros(dimensions), cov=mass_matrix)
    momentum_curr = momentum  # copy momentum for comparison
    position_prev = current_position  # save last position for later

    # perform leapfrog steps
    # TODO: check for correct update formula
    for i in range(step_count):
        # perform half update step of momentum
        momentum_curr = momentum_curr + epsilon * log_posterior_gradient(
            current_position, data, labels, sigma=10) / 2
        # perform full update of position variables
        current_position = current_position + epsilon \
                           * np.matmul(mass_matrix_inv, momentum_curr)
        # perform another half step of momentum update
        momentum_curr = momentum_curr + epsilon * log_posterior_gradient(
            current_position, data, labels, sigma=10) / 2

    # reverse the momentum for reversibility
    momentum_curr *= -1

    # compute prev. and current Hamiltonian
    # TODO: make sure this formula is correct
    hamiltonian_before = -1 * log_posterior(position_prev, data, labels, sigma=10) \
                         + -.5 * momentum @ mass_matrix_inv @ momentum
    hamiltonian_proposal = -1 * log_posterior(current_position, data, labels, sigma=10) \
                           + -.5 * momentum_curr @ mass_matrix_inv @ momentum_curr

    # perform Metropolis-Hastings accept/reject step
    acceptance_prob = np.min([1, np.exp(hamiltonian_before - hamiltonian_proposal)])
    assert not np.isnan(acceptance_prob), "nan value occurred in acceptance " \
                                          "probability. likely a numerical overflow."
    if uniform.rvs() < acceptance_prob:
        position_new = current_position
    else:
        position_new = position_prev

    return position_new, acceptance_prob
