"""Students alcohol consumption example specific functions.

# TODO: name what functions etc.
"""

import numpy as np


def log_posterior(theta: np.ndarray, data: np.ndarray,
                  labels: np.ndarray, **kwargs):
    """Defines the log. posterior distribution from which to sample.

    Implementation of a proportional posterior to binomial likelihood with a
    normal prior to the coefficients.

    :param theta: parameter vector incl. sigma
    :param data: complete data set
    :param labels: labels
    :param kwargs: holding sigma2_beta, alpha and beta for the IG
    :returns: evaluated log. posterior
    """
    # theta includes both beta and sigma parameters
    coeff = theta[:-1]
    sigma2 = theta[-1]**2 + .000000001  # in case sigma = 0

    num_samples = data.shape[0]
    covar_inv = np.linalg.inv(np.eye(num_samples) * sigma2)
    covar_beta_inv = np.linalg.inv(np.eye(coeff.__len__()) * kwargs["sigma2_beta"])

    # given by: ... TODO
    log_likelihood = -.5 * num_samples * np.log(sigma2) \
                     - .5 * (labels - data @ coeff).T @ covar_inv @ (labels - data @ coeff)
    assert log_likelihood.shape == (), f"got: {log_likelihood.shape}"
    assert not np.isnan(log_likelihood), f"sigma2: {sigma2}, " \
                                         f"y-Xb: {labels - data @ coeff}, " \
                                         f"Sigma-1: {covar_inv}"

    # given by: ... TODO
    log_priori_beta = -.5 * coeff.T @ covar_beta_inv @ coeff
    assert log_priori_beta.shape == (), f"got: {log_priori_beta.shape}"
    assert not np.isnan(log_priori_beta)

    # given by: ... TODO
    log_priori_sigma = -1 * (kwargs["alpha"] + 1) * np.log(sigma2) \
                       - kwargs["beta"] / sigma2
    assert log_priori_sigma.shape == (), f"got: {log_priori_sigma.shape}"
    assert not np.isnan(log_priori_sigma)

    return log_likelihood + log_priori_beta + log_priori_sigma


def log_posterior_grad(theta: np.ndarray, data: np.ndarray,
                       labels: np.ndarray, **kwargs):
    """Defines the gradient to a posterior with binomial likelihood and
    gaussian prior.

    :param theta: parameter vector incl. sigma
    :param data: complete data set
    :param labels: labels
    :param kwargs: distribution parameters for priors as sigma2_beta, alpha and beta
    :returns: evaluated gradient of log. posterior
    """
    # theta includes both beta and sigma parameters
    coeff = theta[:-1]
    sigma2 = theta[-1]**2

    # if np.isnan(coeff).any(): print("coeff")
    # if np.isnan(sigma2).any(): print("sigma")

    num_samples = data.shape[0]
    covar_inv = np.linalg.inv(np.eye(num_samples) * sigma2)
    covar_beta_inv = np.linalg.inv(np.eye(coeff.__len__()) * kwargs["sigma2_beta"])

    # given by: ... TODO
    d_log_likelihood_coeff = labels.T @ covar_inv @ data - coeff.T @ data.T @ data \
                             - coeff.T @ covar_beta_inv
    assert d_log_likelihood_coeff.shape == coeff.shape, f"mismatch, " \
                                                        f"got {d_log_likelihood_coeff.shape} " \
                                                        f"vs. {coeff.shape}"

    # given by: ... TODO
    d_log_likelihood_sigma2 = -2 * (num_samples/2 + kwargs["alpha"] + 1) / sigma2 \
                              - .5 * (labels.T @ labels - 2 * labels.T @ data @ coeff) \
                              - 2*kwargs["beta"] / (sigma2 * np.nan_to_num(np.sqrt(sigma2), nan=1))
    if np.isnan(d_log_likelihood_sigma2): print("now\n", d_log_likelihood_sigma2)
    assert d_log_likelihood_sigma2.shape == sigma2.shape, f"mismatch, " \
                                                          f"got {d_log_likelihood_sigma2.shape} " \
                                                          f"vs. {sigma2.shape}"

    return np.array([*d_log_likelihood_coeff, d_log_likelihood_sigma2]) / num_samples
