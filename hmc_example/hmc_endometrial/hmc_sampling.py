"""Setup and run HMC sampling.

Using a basic HMC step to traverse the phase space for the set time using the
provided potential energy.
"""

import numpy as np
from functools import partial
from tqdm import tqdm
import _thread
from hmc_step import hmc_step
from endometrial_functions import log_posterior, log_posterior_grad

tqdm = partial(tqdm, position=0, leave=True)


def run_hmc(init_params: np.ndarray, total_iterations: int,
            mass_matrix: np.ndarray, epsilon: float,
            leapfrog_steps: int, data: np.ndarray,
            labels: np.ndarray) -> [np.ndarray]:
    # accumulation array of sampling results; samples & acceptance probabilities
    acc_simulation = np.zeros((total_iterations, init_params.shape[0]))
    acc_acceptance = np.zeros((total_iterations, 1))

    # run HMC sampler
    position = init_params
    for idx in tqdm(range(total_iterations)):
        # print(f"Executing {idx+1} iterations. ".ljust(80, "="))
        # TODO: when implementing chains, alter eps. and L a little
        position, acceptance_prob = hmc_step(log_posterior=log_posterior,
                                             log_posterior_gradient=log_posterior_grad,
                                             data=data, labels=labels,
                                             mass_matrix=mass_matrix,
                                             current_position=position,
                                             epsilon=epsilon,
                                             step_count=leapfrog_steps)
        acc_simulation[idx] = position
        acc_acceptance[idx] = acceptance_prob

    return acc_simulation, acc_acceptance


def run_hmc_sampler(init_params: np.ndarray, total_iterations: int,
                    burnin: int, mass_matrix: np.ndarray, epsilon: float,
                    leapfrog_steps: int, data: np.ndarray,
                    labels: np.ndarray) -> [np.ndarray]:
    """Run the HMC sampling routing.

    :param init_params: initial params to start the sampling with
    :param total_iterations: total iterations of HMC steps
    :param burnin: number of iterations to drop before doing inference
    :param mass_matrix: mass matrix for kinetic energy
    :param epsilon: leapfrog integration step size
    :param leapfrog_steps: number of leapfrog steps per HMC step
    :param data: data
    :param labels: labels
    :return: history of samples and acceptance probabilities
    """
    # define accumulators for chains
    acc_chains_samples = np.zeros((init_params.shape[0], total_iterations,
                                   init_params.shape[1]))
    acc_chains_acceptance = np.zeros((init_params.shape[0], total_iterations, 1))

    for t in tqdm(range(init_params.shape[0])):
        samples, acceptance_ratios = run_hmc(init_params=init_params[t],
                                             total_iterations=total_iterations,
                                             mass_matrix=mass_matrix,
                                             epsilon=epsilon,
                                             leapfrog_steps=leapfrog_steps,
                                             data=data, labels=labels)
        acc_chains_samples[t] = samples
        acc_chains_acceptance[t] = acceptance_ratios

    return acc_chains_samples, acc_chains_acceptance
