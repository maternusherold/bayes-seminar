"""Providing utility functions for plotting """


import matplotlib.pyplot as plt
import numpy as np

# TODO: adapt the plotting to all variables
def plot_histograms(samples: np.ndarray) -> plt.figure:
    """Plots the samples as histograms grouped by chain.

    :param samples: array of chains which hold the samples
    :returns: figure object to be adjusted further if needed
    """
    chains = samples.shape[0]
    num_features = samples.shape[2]

    #  compute quantiles
    # quantile_lower = np.quantile(samples, .05, axis=1)
    # quantile_upper = np.quantile(samples, .94, axis=1)

    # create fig. size rel. to feature count
    fig = plt.figure(figsize=(16, 4 * num_features))

    plotting_rows = np.ceil(num_features / 2)
    for feature in range(num_features):
        ax = fig.add_subplot(plotting_rows, 2, feature + 1)
        for c in range(chains):
            ax.hist(samples[c, :, feature], 100, label=f"chain {c+1}", alpha=.85)
            # ax.axvline(quantile_lower[c, feature])
            # ax.axvline(quantile_upper[c, feature])
        ax.set_title(r"$\beta$"+f"{feature}", fontsize=24)
        ax.legend()

    return fig


def plot_traces(samples: np.ndarray) -> plt.figure:
    """Plot trace plots of samples per chain.

    :param samples: samples organised in chains
    :returns: figure object to be adjusted further if needed
    """
    chains = samples.shape[0]
    num_features = samples.shape[2]

    # create fig. size rel. to feature count
    fig = plt.figure(figsize=(18, 4 * num_features))

    plotting_rows = np.ceil(num_features / 2)
    for feature in range(num_features):
        ax = fig.add_subplot(plotting_rows, 2, feature + 1)
        for c in range(chains):
            ax.plot(samples[c, :, feature], label=f"chain {c+1}")
            ax.legend()
        ax.set_title(r"$\beta$"+f"{feature}", fontsize=24)
        ax.legend()

    return fig
