% =============================================================================
% Introduction deals with the motivation for the topic, i.e. efficient sampling 
% in high dimensional spaces. 
% =============================================================================

For most statistical tasks exact inference is impossible due to the dimensionality of the task itself. Especially in the Bayesian statistics, where the posterior distributions is of high interest, the denominator (\textit{evidence}), see \cref{eq:simple-bayes}, of such is most often intractable due to its high dimensional integral. However, the proportional posterior is, which equals the true posterior up to a constant. 

For certain, simpler cases, a conjugate prior distribution to the likelihood leads to a posterior of a known form as it is in the same family of probability distributions. However, picking a prior matching the likelihood to ease the derivation of the posterior prunes the power of the final model and also leads to distrust in Bayesian statistical \cite{gelman-objections-bayes-stats}. 

\begin{equation}
    \label{eq:simple-bayes}
    p(\theta \vert X) = \frac{\mathcal{L}(X \vert \theta) p(\theta)}{p(X)}
\end{equation}

Further, deriving the posterior purely from the given equations yields more flexibility in the method itself as the statistician is free to choose from all possible distributions. 

Means to still gain information from intangible posteriors, due to high dimensionality and/or lacking conjugate priors, can be sampling methods. Such try to explore the posterior distribution by, for example, upper bounding the target distribution by a simple distribution $\pi(z)$, from which it can be sampled - see rejection sampling \cite{patternRecognition}. 

Monte Carlo methods have proven themselves as fast and reliable sampling methods. While toy examples show the approximation of $\pi$ for simple Monte Carlo sampling, more sophisticated methods are equipped with an exploration technique only depending on a set number $k$ of the most recent steps and thus being independent of the past steps, the Markov Chain of order $k$. Being only dependent on a view previous samples makes stating and evaluating a Markov Chain easy while providing means of traversing the target space randomly - the \textit{random walk}. This class of methods is called Markov Chain Monte Carlo. 

For this kind of sampling to work, the Markov Chain has to leave the distribution \textit{invariant}, i.e. a step of the Markov Chain does not change the distribution. 

% TODO: define invariance 

Building on the notion of invariance, another notable property of Markov Chain Monte Carlo (MCMC) methods is \textit{ergodicity}. A Markov Chain is said to fulfill this property, if the currently sampled distribution converges to the desired target distribution when the steps $k \to \infty$, regardles of the initialization. 

Using those two properties lead to the asymptotic behavior of the sampling method, when sampling from the distribution $\pi$.

\begin{equation}
    \label{eq:mcmc-asymptotic}
    \pi_k \to \pi^*,\quad k \to \infty
\end{equation}

More specific, estimating the expectation of a function $f$ over a probability measure $\pi$ using MCMC is setup a follows:

\begin{align}
    \label{eq:mcmc-ecpectation}
    \mathbb{E}_{\pi}[f] &= \int_{\Omega} \pi(\omega)f(\omega) d\omega  \\
    \hat{f}_k &= \frac{1}{k} \sum^k_{i=0} f(\omega_i)   \\
    \lim_{k \to \infty} \hat{f}_k &= \mathbb{E}_{\pi}[f] \label{eq:mcmc-asymptotic-example}
\end{align}

This setup is fairly simple, but \cref{eq:mcmc-asymptotic,eq:mcmc-asymptotic-example} facilitate the need for efficient versions of the MCMC method. It is important to get a clear understanding of the target distribution after only a finite number of transactions as the asymptotic convergence is impractical.

The efficiency in exploration of an algorithm is even more important when increasing the dimensionality of the target space. While a usual introductory example, e.g. the boston housing data set \cite{dataset-boston} in fourteen dimensions, are lower dimensional, real use cases scale easily to far higher dimensions. See biological or image analysis for example. However, observations in such a dataset would have to exceed an amount that scales exponentially to yield a fairly dense coverage of the space. Assuming $10$ samples per unit in each dimension are considered as dense. Then, the sample size grows with $\mathcal{O}(10^D)$, where $D$ is the dimension. Another illustration is given in \cref{fig:high-dim-samples}. Usually, such large numbers of observations are not the case leading to \textit{sparse} spaces where the data itself only occupies very little and thus is hard to find. 

\begin{figure}
    \centering
    \includegraphics[width=1.\linewidth]{ratio_samples_in_sphere.png}
    \caption{Including a hyper sphere inside a hyper cube, where the sphere has the cube's edge length as its diameter, to demonstrate the sparsity of high dimensions. When sampling uniformly in all available dimensions, a fast decreasing number of samples actually fall inside the sphere. Note, the observation is independent of the amount of samples drawn.}
    \label{fig:high-dim-samples}
\end{figure}

Assuming a certain, fairly small, region in the space to yield most of the information about the distribution, it is important to explore such regions efficiently, once they are found. The idea stems form the observation of high density usually being coupled with low volume and high volume being coupled with low density, such that a small part, having both, descent volume and density, contributes a lot to the statistic being computed. Such regions are often called \textit{typical sets}, e.g. \cite{betancourtConceptional}, to emphasize the tensions between regions of high volume and regions with high density. It is the aim of many methods to explore such regions efficiently.
 
The most well known MCMC method, the Metropolis-Hastings (MH) method, fails w.r.t. efficient exploration due to its aforementioned strength, the (local) random walk. By such the random steps to pick the next sample are described, as all the steps together can be seen as a walk through the space. Two pathologies can be derived for the local random walk:

\begin{enumerate}
    \item pure random proposals of next steps lead to a high rejection rate in the MH step as leaving the typical set is likely
    \item as the step size should not exceed the square root of the smallest eigenvalue (will never be able to make a step in that direction otherwise), exploring the direction of the larger principal components will take very long, \cite{mcmcHandbook}
\end{enumerate}

Not to mention the fixed step size through which the MH method will just step over regions of high curvature. Therefore, it is elementary to eliminate the local random walk behavior and incorporate knowledge of the typical set to pick the direction of the next step. 

Methods using this informations are the here presented Hamiltonian Monte Carlo (HMC) or an advancement of the same, the No-U-Turn sampler (NUTS).
