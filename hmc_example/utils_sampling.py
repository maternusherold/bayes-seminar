"""Several useful metrics and utilities regarding the evaluation of the chains. """


import numpy as np


def empirical_variance(samples: np.ndarray):
    """Compute the empirical variance within a chain per parameter.

    :param samples: samples of the same parameters with different chains
    :returns: with-in chain variances per parameter
    """
    num_samples = samples.shape[1]

    # compute the parameter means per chain and add dimension
    # to fit further computations
    sample_means = np.mean(samples, axis=1)
    sample_means = np.expand_dims(sample_means, axis=1)

    # compute sum of squared differences
    squared_mean_variation = np.sum((samples - sample_means) ** 2, axis=1)
    assert squared_mean_variation.shape == (samples.shape[0], samples.shape[2]), \
        f"got: {squared_mean_variation.shape} vs. {samples.shape[0], samples.shape[2]}"

    return 1 / (num_samples - 1) * squared_mean_variation


def mean_of_chains(samples: np.ndarray):
    """Computes the mean of parameter means across chains.

    :param samples: TODO
    :returns: mean of parameter across all chains
    """
    # compute the parameter means per chain and add dimension
    # to fit further computations
    sample_means = np.mean(samples, axis=1)
    chain_means = sample_means.mean(axis=0)
    assert chain_means.shape[0] == samples.shape[2], f"got: {chain_means.shape} " \
                                                  f"vs. {samples.shape[2]}"
    return chain_means


def within_variance(samples: np.ndarray):
    """Computes the with-in-chain variance per parameter.

    :param samples: samples of the same parameters with different chains
    :returns: with-chain variance per parameter
    """
    num_chains = samples.shape[0]
    emp_var = empirical_variance(samples)
    emp_var_summed = np.sum(emp_var, axis=0)
    assert emp_var_summed.shape[0] == samples.shape[2], f"got: {emp_var_summed.shape} " \
                                                     f"vs. {samples.shape[2]}"

    return (1 / num_chains) * emp_var_summed


def between_variance(samples: np.ndarray):
    """Computes the between-chain variance per parameter.

    :param samples: samples of the same parameters with different chains
    :returns: between-chain variance per parameter
    """
    num_samples = samples.shape[1]
    num_chains = samples.shape[0]

    mean_of_means = mean_of_chains(samples)
    sample_means = np.mean(samples, axis=1)
    # sample_means = np.expand_dims(sample_means, axis=1)

    variance_between = np.sum((sample_means - mean_of_means)**2, axis=0)
    assert variance_between.shape[0] == samples.shape[2], f"got: {variance_between.shape} " \
                                                       f"vs. {samples.shape[2]}"
    return num_samples / (num_chains - 1) * variance_between


def gelman_rubin(samples: np.ndarray) -> float:
    """Computes the Gelman-Rubin diagnostic measure

    The metric estimate of convergence based on the variance of an estimated
    parameter theta between chains, and the variance within a chain. It is
    interpreted as the factor by which the variance in the estimate might be
    reduced with longer chains.

    :param samples: samples of the same parameters with different chains
    :returns: PSRE for all parameters across chains
    """
    num_samples = samples.shape[1]

    # compute variances
    variance_within = within_variance(samples)
    variance_between = between_variance(samples)

    # compute empirical variance as with-in and between variances per parameter
    parameter_variance = (1 - (1 / num_samples)) * variance_within \
                         + (1 / num_samples) * variance_between
    assert parameter_variance.shape[0] == samples.shape[2], f"got: {parameter_variance.shape} " \
                                                         f"vs. {samples.shape[2]}"

    return np.sqrt(parameter_variance / variance_within)


def quantiles_per_chain(samples: np.ndarray, burnin: int = 0,
                        quantiles: np.ndarray = [.025, .25, .5, .75, .975]):
    """Compute samples quantiles per chain.

    :param samples: TODO
    :param burnin: burnin cut; excl.
    :param quantiles: list of quantiles to compute
    """
    return np.quantile(samples[:, burnin:, :], quantiles,
                       axis=1).T
