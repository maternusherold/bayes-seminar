# Elaboration of Hamiltonian Monte Carlo 

*... code and paper work for Bayes Seminar at LMU - binaries and larger files are tracked via Git LFS*

**topic:** elaboration of the Hamiltonian Monte Carlo (HMC) algorithm

**goal:** describe and compare the HMC algorithm and provide an example on how it works; implemented in a desired language

## Contents

```
.
├── hmc_example
│   ├── data
│   ├── examples
│   ├── hmc_boston
│   ├── hmc_endometrial
│   ├── hmc_students_alcohol
│   ├── README.md
│   ├── __init__.py
│   ├── hamiltonian_monte_carlo.py
│   ├── integration_methods.py
│   ├── integration_techniques
│   ├── __pycache__
│   └── utils_sampling.py
├── koreferat
│   ├── chapters
│   ├── figures
│   └── main.pdf
├── presentation
│   ├── chapters
│   ├── figures
│   ├── main.bib
│   ├── main.pdf
│   └── plots
├── README.md
└── seminar_paper
    ├── chapters
    ├── figures
    ├── main.bib
    ├── main.pdf
    ├── main.tex
    └── structure.tex

```

