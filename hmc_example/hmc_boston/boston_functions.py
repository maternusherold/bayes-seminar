"""Boston example specific functions.

# TODO: name what functions etc.
"""

import numpy as np
from scipy.stats import multivariate_normal as mvnorm
from scipy.stats import invgamma


def log_posterior(theta: np.ndarray, data: np.ndarray,
                  labels: np.ndarray):
    """Defines the log. posterior distribution from which to sample.

    Implementation of a proportional posterior to a normally distributed variable
    with another normal prior and an inverse gamma prior.

    :param theta: parameter vector
    :param data: complete data set 
    :param labels: labels
    :returns: evaluated log. posterior 
    """

    # select coefficients and sigma parameter
    coefficients = theta[:-1]
    sigma = theta[-1]  # std. dev.
    sigma2 = sigma ** 2  # square for variance

    # parameters for data likelihood --
    # set covariance matrix for labels
    covariance_data = np.eye(labels.__len__()) * sigma2

    # parameters for coeff. prior -----
    # set covariance matrix for coefficients
    coefficients_mu = np.zeros(coefficients.__len__())
    # FIXME: adapt to data - see Gelman how
    coefficients_cov = np.eye(coefficients.__len__())

    # parameters for sigma prior ------
    # set parameters for sigma prior, i.e. alpha, beta
    alpha, beta = 1., .001

    # compute mu
    mu = np.matmul(data, coefficients)

    # in the case of undefined variance
    if sigma < 0:
        return_value = -np.inf
    else:

        log_likelihood = mvnorm.logpdf(labels, mean=mu, cov=covariance_data)
        log_prior_coeff = mvnorm.logpdf(coefficients, mean=coefficients_mu, cov=coefficients_cov)
        log_prior_sigma = invgamma.logpdf(sigma2, a=alpha, scale=beta)

        # set to return the log posterior
        return_value = log_likelihood + log_prior_coeff + log_prior_sigma

    return return_value


def log_posterior_gradient_numerical(theta: np.ndarray,
                                     data: np.ndarray,
                                     labels: np.ndarray,
                                     epsilon: float = .0000000000001):
    """Gradient of the log. posterior numerically using symmetric difference.

    :param theta: parameter vector
    :param data: complete data set
    :param labels: labels
    :param epsilon: gradient parameter defining the symmetric difference
    :returns: evaluated gradient of log. posterior
    """
    # define gradients
    grads = np.zeros(shape=theta.shape)

    # iterate over variables and compute grad. per direction
    for idx, var in enumerate(theta):
        # reset highs and lows of variable vector
        theta_high = np.copy(theta)
        theta_low = np.copy(theta)

        # only change the variable in the current direction
        theta_high[idx] = theta[idx] + epsilon
        theta_low[idx] = theta[idx] - epsilon

        # approximating the gradient with symmetric difference
        grads[idx] = (log_posterior(theta_high, data, labels)
                      - log_posterior(theta_low, data, labels)) / (2 * epsilon)

        # check for erroneous gradients
        if np.abs(grads[idx]) == np.inf:
            grads[idx] = 0

    return grads


def proportional_log_posterior(theta, data, y):
    coeff = theta[:-1]
    sigma = theta[-1]
    alpha, beta = 1.0, .0001
    n = data.shape[0]
    cov_data = np.linalg.inv(np.eye(n) * sigma)
    cov_beta = np.linalg.inv(np.eye(coeff.__len__()))
    log_post = -2 * (.5 * n + 1 + alpha) * np.log(sigma) \
               - .5 * (y - data @ coeff).T @ cov_data @ (y - data @ coeff) \
               - .5 * coeff.T @ cov_beta @ coeff \
               - beta / sigma ** 2
    return log_post


def proportional_log_posterior_grad(theta, data, y):
    coeff = theta[:-1]
    sigma = theta[-1]
    alpha, beta = 1.0, .0001
    n = data.shape[0]
    cov_data_inv = np.linalg.inv(np.eye(n) * sigma)
    cov_beta_inv = np.linalg.inv(np.eye(coeff.__len__()))

    grads_coeff = (y.T @ cov_data_inv @ data - coeff.T @ data.T @ cov_data_inv @ data
                   - coeff.T @ cov_beta_inv) / data.shape[0]
    grad_sigma = (-2 * (alpha + n / 2 + 1) / sigma
                  + .5 * (y - data @ coeff).T @ (y - data @ coeff) / sigma ** 2
                  + 2 * beta / sigma ** 3) / data.shape[0]

    assert grads_coeff.shape == coeff.shape
    assert grad_sigma.shape == sigma.shape

    return np.array([*grads_coeff, grad_sigma])
